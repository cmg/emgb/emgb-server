#!/bin/bash
DATA_DIR=/data
IMPORT_DIR=/import

MAPPING_FILE='index.json'
GENE_MAPPING_FILE='gene_mapping.json'

if [[ ! -z "${2+x}" ]]; then
  if [[ "${2}" = 'nobins' ]]; then
    echo "Using old mapping without bins"
    MAPPING_FILE='index-nobins.json'
  else
    GENE_MAPPING_FILE="gene_mapping-${2}.json"
    echo ""
    echo "[INFO] using non-standard gene mapping: ${GENE_MAPPING_FILE}"
    echo ""
  fi
fi

if [[ ! -f "${DATA_DIR}/${1}.genes.json.gz" ]]; then
  echo "Error: ${1}.genes.json.gz does not exist inside data folder."
  exit 1
fi

echo -n "Deleting index '${1}': "; curl -s -XDELETE http://elasticsearch:9200/${1} >/dev/null && echo "OK" || echo "Failed"
echo -n "Creating index and settings for '${1}': "; curl -sS -XPUT http://elasticsearch:9200/${1}/ --data-binary @${IMPORT_DIR}/${MAPPING_FILE} -H "Content-Type: application/json" >/dev/null && echo "OK" || echo "Failed"
if [[ ! "${2}" = 'nobins' ]]; then
  echo -n "Writing bin mapping for '${1}': "; curl -sS -XPUT http://elasticsearch:9200/${1}/_mapping/bin --data-binary @${IMPORT_DIR}/bin_mapping.json -H "Content-Type: application/json" >/dev/null && echo "OK" || echo "Failed"
fi
echo -n "Writing contig mapping for '${1}': "; curl -sS -XPUT http://elasticsearch:9200/${1}/_mapping/contig --data-binary @${IMPORT_DIR}/contig_mapping.json -H "Content-Type: application/json" >/dev/null && echo "OK" || echo "Failed"
echo -n "Writing gene mapping for '${1}': "; curl -sS -XPUT http://elasticsearch:9200/${1}/_mapping/gene --data-binary @${IMPORT_DIR}/${GENE_MAPPING_FILE} -H "Content-Type: application/json" >/dev/null && echo "OK" || echo "Failed"
echo -n "Writing pathway mapping for '${1}': "; curl -sS -XPUT http://elasticsearch:9200/${1}/_mapping/pathway --data-binary @${IMPORT_DIR}/pathway_mapping.json -H "Content-Type: application/json" >/dev/null && echo "OK" || echo "Failed"
echo ""
echo "Importing pathways for '${1}' ..."
zcat ${IMPORT_DIR}/emgb.pathways.json.gz | curl -sS -H 'Content-Type: application/x-ndjson' -XPOST http://elasticsearch:9200/${1}/pathway/_bulk --data-binary @- | jq '.items[].index.error.reason | select(. != null)'
echo -e "\nDone"
echo ""
if [[ ! "${2}" = 'nobins' ]]; then
  echo "Importing bins for '${1}' ..."
  zcat ${DATA_DIR}/${1}.bins.json.gz | parallel --gnu -j 1 --blocksize 10M -L 100 --no-notice --pipe "curl -sS -H 'Content-Type: application/x-ndjson' -XPOST http://elasticsearch:9200/${1}/bin/_bulk --data-binary @- | jq '.items[].index.error.reason | select(. != null)'"
  echo -e "\nDone"
  echo ""
fi
echo "Importing contigs for '${1}' ..."
zcat ${DATA_DIR}/${1}.contigs.json.gz | parallel --gnu -j 4 --blocksize 10M -L 500 --no-notice --pipe "curl -sS -H 'Content-Type: application/x-ndjson' -XPOST http://elasticsearch:9200/${1}/contig/_bulk --data-binary @- | jq '.items[].index.error.reason | select(. != null)'"
echo -e "\nDone"
echo ""
echo "Importing genes for '${1}' ..."
zcat ${DATA_DIR}/${1}.genes.json.gz | parallel --gnu -j 4 --blocksize 10M -L 500 --no-notice --pipe "curl -sS -H 'Content-Type: application/x-ndjson' -XPOST http://elasticsearch:9200/${1}/gene/_bulk --data-binary @- | jq '.items[].index.error.reason | select(. != null)'"
echo -e "\nDone"

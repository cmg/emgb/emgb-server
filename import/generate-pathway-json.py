#!/usr/bin/env python3
from PIL import Image
import base64, json, re, gzip

KEGG_DIR = '/tmp/kegg-mirror-2022-12/'
json_gz_filename = 'emgb.pathways.json.gz'

pathways = {}

with open(KEGG_DIR + '/pathway/pathway.list') as pathway_list_file:
  pathway_list_raw = pathway_list_file.readlines()
pathway_list_raw = [line.strip() for line in pathway_list_raw]

for line in pathway_list_raw:
  if line.startswith('#') and not line.startswith('##'):
    group = line[1:]
  if line.startswith('##'):
    subgroup = line[2:]
  if not line.startswith('#'):
    id, title = line.split('\t')
    with open(KEGG_DIR + '/pathway/map/map%s.png' % id, 'rb') as img_file:
      img_pil = Image.open(img_file)
      w, h = img_pil.size
      img_file.seek(0)
      img = base64.b64encode(img_file.read()).decode('UTF-8')
    rects = []
    with open(KEGG_DIR + '/pathway/map/map%s.conf' % id, 'r') as img_conf:
      for line in img_conf:
        # rect (268,376) (314,393)        /dbget-bin/www_bget?K01222+K01223+3.2.1.86+R05134       K01222 (E3.2.1.86A), K01223 (E3.2.1.86B), 3.2.1.86, R05134
        m = re.search('rect \(([0-9]{1,4}),([0-9]{1,4})\) \(([0-9]{1,4}),([0-9]{1,4})\)\t'
                      '.*\?(.*)\t.*', line.strip())
        if m:
          kos = m.group(5).split('+')
          rect = {
            'x1': int(m.group(1)),
            'y1': int(m.group(2)),
            'x2': int(m.group(3)),
            'y2': int(m.group(4)),
            'kos': kos
          }
          if not kos[0].startswith('map'):
            rects.append(rect)

    pathways[id] = {
      'pathid': 'map' + id,
      'title': title,
      'group': group,
      'subgroup': subgroup,
      'rects': rects,
      'w': int(w),
      'h': int(h),
      'img': img
    }

with gzip.open(json_gz_filename, 'wb') as json_gz:
  for id, pathway in pathways.items():
    json_gz.write(b'{"index":{}}')
    json_gz.write(b'\n')
    json_gz.write(json.dumps(pathway).encode('UTF-8'))
    json_gz.write(b'\n')
print('Output written to file \'%s\'' % json_gz_filename)

# Exploratory MetaGenome Browser (EMGB)

>Note: The current state supports private installations only.
Functionality for public as well as role-based access is a
work-in-progress (May 2024).

---

- [Exploratory MetaGenome Browser](#exploratory-metagenome-browser-emgb)
  * [Quick Start](#quick-start)
    + [Run EMGB using Docker Compose](#run-emgb-using-docker-compose)
      - [Import dataset](#import-dataset)
      - [Build BLAST+ database](#build-blast-database)
  * [Installation commands for setup on emgb.cebitec](#installation-commands-for-setup-on-emgbcebitec)
    + [EMGB instance setup](#emgb-instance-setup)
    + [Apache reverse proxy setup](#apache-reverse-proxy-setup)
    + [Data import](#data-import)
    + [Sending secrets](#sending-secrets)

## Quick Start
### Run EMGB using Docker Compose
```
git clone https://gitlab.ub.uni-bielefeld.de/cmg/emgb/emgb-server
cd emgb-server/

mkdir data/
mkdir esdata/
chmod 777 esdata/
mkdir blast/db/
mkdir -p targetdb/db/

docker-compose up
```

#### Import dataset
```
cd emgb-server/
./prepare-import.sh
```
Put the following files into the `data/` directory.

- `mydataset.genes.json.gz`
- `mydataset.contigs.json.gz`
- `mydataset.bins.json.gz`

```
docker-compose exec import /import/import.sh mydataset
```


#### Build BLAST database
Put combined _db.faa_ of all datasets into `blast/db/`.

```
docker-compose exec blast makeblastdb.sh
```


## Installation commands for setup on emgb.cebitec

Example parameters:

| Parameter | Value | Comment |
| --- | ---: | --- |
| EMGB instance name | `magdeburg` | Please stick to lowercase letters and numbers. |
| Localhost listen port | `8100` | Is incremental, see `/mnt/ports` for the list of used ports. |
| Docker subnet third octet | `24` | Is also incremental, see `/mnt/ports`. |
| Disk | `/mnt-volume/` | The other option would be the ephemeral disk on `/mnt/`. Keep an eye on the available disk space. |


### EMGB instance setup

On `emgb.cebitec` as user `ubuntu` run:
```
cd /mnt-volume/
git clone https://gitlab.ub.uni-bielefeld.de/cmg/emgb/emgb-server emgb2-magdeburg
cd emgb2-magdeburg

mkdir data/
mkdir esdata/
chmod 777 esdata/
mkdir blast/db/
./prepare-import.sh
```

Re-use existing Docker images for services that seldomly change their image:
```
docker tag emgb2sra_import emgb2magdeburg_import
docker tag emgb2sra_blast emgb2magdeburg_blast
docker tag emgb2sra_targetdb emgb2magdeburg_targetdb
```
This is a shortcut. In this case the `sra` EMGB project images are re-used.
The images can of course also be built from scratch using docker-compose.

Configure the listen port and subnet octet parameters inside `docker-compose.yml`:
```
sed -i -e 's/127\.0\.0\.1:80:80/127.0.0.1:8100:80/' -e 's#172\.100\.0\.0/24#172.100.24.0/24#' -e 's#172\.101\.0\.0/24#172.101.24.0/24#' docker-compose.yml
```

Systemd is used to start/stop all the EMGB instances. Run the command below to create,
start as well as autostart the EMGB instance `magdeburg` on `/mnt-volume/`.
```
sudo systemctl enable --now docker-compose-volume@magdeburg
```
Always use `docker-compose@...` for `/mnt/` but `docker-compose-volume@...` for `/mnt-volume/`.

### Apache reverse proxy setup

Use a password generator (e.g. `pwgen 12` to generate a password).
Set the password for the HTTP Basic Auth user with:
```
htpasswd -c apache-magdeburg-htpasswd magdeburg
```

This creates the file `apache-magdeburg-htpasswd` which is used by Apache.

Add the section below to `/etc/apache2/sites-available/emgb.conf`
at the end of `<VirtualHost *:80>...</VirtualHost>` to configure Apache:

```
        RewriteRule "^/magdeburg$" "/magdeburg/" [R,L]
        ProxyPass /magdeburg/ http://localhost:8100/ retry=5
        ProxyPassReverse /magdeburg/ http://localhost:8100/
        RequestHeader unset Referer
        RequestHeader unset Origin

        <location /magdeburg>
                AuthType Basic
                AuthName "magdeburg"
                AuthUserFile "/mnt-volume/emgb2-magdeburg/apache-magdeburg-htpasswd"
                Require valid-user
                RequestHeader unset Authorization
        </location>
```

Reload Apache to apply the new config section:
```
sudo systemctl reload apache2
```

This Apache is behind another proxy on the BiBiServ that forwards the requests to
`emgb.cebitec` to this one.

### Data import

Download and import data into the `magdeburg` EMGB instance:
```
cd /mnt-volume/emgb2-magdeburg/data
mc find biogas/magdeburg/illumina/results/ --name "*.json.gz" --exec "mc cp {} ."
docker-compose exec import /import/import.sh hoda_illumina

mc cat biogas/magdeburg/illumina/results/prodigal.faa.gz | zcat >> ../blast/db/db.faa
docker-compose exec blast makeblastdb.sh
```

### Sending secrets

Access the secret sharing service on `emgb.cebitec` via SSH tunnel:
```
ssh -L 7143:localhost:7143 [...]
```
Open in your browser: <http://localhost:7143/secret>
Generate one or more secrets and copy the URLs for sharing.
The links only expire on reboot of `emgb.cebitec`.
The secrets can only be viewed once.
The secret sharing service is based on:
<https://gitlab.ub.uni-bielefeld.de/c/sendsecret>

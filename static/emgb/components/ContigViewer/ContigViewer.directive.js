/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.ContigViewer')
    .directive('contigViewer', ContigViewer);

  function ContigViewer(ElasticSearch, $rootScope, $timeout, KO_COG_MAP, COG_LETTER_MAP, PHYLO_TREE_RANK_LABELS, GENBANK_EXPORT_ENABLED) {
    return {
      scope: {
        contigid: '=',
        geneid: '=',
        dataset: '=',
        pgtbl: '='
      },
      restrict: "E",
      templateUrl: 'components/ContigViewer/contig-viewer.html',
      controllerAs: 'cvCtrl',
      bindToController: true,
      link: function (scope, element, attrs) {
      },
      controller: function ($scope) {
        var cvCtrl = this;


        cvCtrl.contig = {};
        cvCtrl.loading = false;
        cvCtrl.longestGeneLength = 0;
        cvCtrl.taxRanks = _.unzip(_.toPairs(PHYLO_TREE_RANK_LABELS))[0];
        cvCtrl.COG_LETTER_MAP = COG_LETTER_MAP;
        cvCtrl.highlight = cvCtrl.pgtbl.contigViewerHighlightSelection;
        cvCtrl.displayCoverage = cvCtrl.pgtbl.contigViewerDisplayCoverage;
        cvCtrl.coverageName = cvCtrl.pgtbl.contigViewerDisplayCoverageForName;
        cvCtrl.covOptionAllSamples = "all samples combined";
        cvCtrl.highlightOptions = [{label: 'COG', value: 'cog', type: 'function'}];
        cvCtrl.taxColorMap;
        cvCtrl.visibleCogs = [];
        cvCtrl.availableCoverageNames = [];
        cvCtrl.coverageInfoAvailable = true;
        cvCtrl.genbankExportEnabled = GENBANK_EXPORT_ENABLED;

        cvCtrl.downloadContigGbk = downloadContigGbk;
        cvCtrl.downloadContigFaa = downloadContigFaa;

        angular.forEach(cvCtrl.taxRanks, function (rank) {
          cvCtrl.highlightOptions.push({label: PHYLO_TREE_RANK_LABELS[rank], value: rank, type: 'taxonomy'});
        });

        cvCtrl.fetch = fetch;

        function fetch() {
          $rootScope.startSpinner();
          cvCtrl.loading = true;
          ElasticSearch.getContig(cvCtrl.dataset.name, cvCtrl.contigid, false, cvCtrl.displayCoverage)
            .then(function (resp) {
              var contigHit = resp.hits.hits[0];
              if (cvCtrl.displayCoverage) {
                cvCtrl.availableCoverageNames = [];
                angular.forEach(contigHit._source['coverage'], function (covObj, i) {
                  cvCtrl.availableCoverageNames.push(covObj['name']);
                });
                if (cvCtrl.availableCoverageNames.length > 0 && cvCtrl.coverageName === '') {
                  cvCtrl.coverageName = cvCtrl.covOptionAllSamples;
                }
                if (cvCtrl.availableCoverageNames.length == 0) {
                  cvCtrl.displayCoverage = false;
                  cvCtrl.coverageInfoAvailable = false;
                } else {
                  cvCtrl.availableCoverageNames.unshift(cvCtrl.covOptionAllSamples);
                }
              }
              ElasticSearch.getContigsGenes(cvCtrl.dataset.name, [cvCtrl.contigid], _.concat(cvCtrl.pgtbl.visibleColumns, cvCtrl.highlight))
                .then(function (respGenes) {
                  cvCtrl.contig = contigHit;
                  cvCtrl.contig.genes = [];
                  var genes = respGenes.hits.hits;
                  genes.sort(function (a, b) {
                    return a._source['start'] - b._source['start'];
                  });
                  postProcessGenes(genes);
                  angular.forEach(genes, function (gene, i) {
                    cvCtrl.contig.genes.push(gene._source);
                  });
                  if (genes.length === 0) {
                    cvCtrl.loading = false;
                  }
                }, function (err) {
                  console.trace(err.message);
                });
            }, function (err) {
              console.trace(err.message);
            })
            .finally(function () {
              $rootScope.stopSpinner();
            });
        }

        function regenerateSvg() {
          d3.select("svg").selectAll("*").remove();
          fetch();
        }

        $scope.$watch(angular.bind(cvCtrl, function () {
          return cvCtrl.highlight;
        }), function (newValue, oldValue) {
          if (newValue !== oldValue) {
            $timeout(function () {
              cvCtrl.pgtbl.contigViewerHighlightSelection = cvCtrl.highlight;
              regenerateSvg();
            }, 0);
          }
        });

        $scope.$watch(angular.bind(cvCtrl, function () {
          return cvCtrl.displayCoverage;
        }), function (newValue, oldValue) {
          if (newValue !== oldValue) {
            $timeout(function () {
              cvCtrl.pgtbl.contigViewerDisplayCoverage = cvCtrl.displayCoverage;
              if (!cvCtrl.loading) {
                regenerateSvg();
              }
            }, 0);
          }
        });

        $scope.$watch(angular.bind(cvCtrl, function () {
          return cvCtrl.coverageName;
        }), function (newValue, oldValue) {
          if (newValue !== oldValue) {
            $timeout(function () {
              cvCtrl.pgtbl.contigViewerDisplayCoverageForName = cvCtrl.coverageName;
              regenerateSvg();
            }, 0);
          }
        });

        function postProcessGenes(genes) {
          var redundantTaxVars = [];
          angular.forEach(genes, function (gene, i) {
            cvCtrl.longestGeneLength = Math.max(cvCtrl.longestGeneLength, gene._source['stop'] - gene._source['start']);
            var koKey;
            if (Array.isArray(gene._source.ko)) {
              koKey = gene._source.ko[0];
            } else {
              koKey = gene._source.ko;
            }
            gene._source._cog = koKey ? KO_COG_MAP[koKey.substring(4)] : '';
            if (!gene._source._cog) {
              gene._source._cog = '';
            }
            redundantTaxVars.push(gene._source[cvCtrl.highlight]);
            if (cvCtrl.highlight === 'cog') {
              var cogChar = gene._source._cog.substring(0, 1);
              gene._source._color = cogChar.toLowerCase();
              cvCtrl.visibleCogs.push(cogChar);
            }
          });
          cvCtrl.visibleCogs = _.uniq(cvCtrl.visibleCogs);
          if (cvCtrl.highlight !== 'cog') {
            var taxVars = _.uniq(_.compact(redundantTaxVars));
            var nums = _.range(1, taxVars.length + 1);
            cvCtrl.taxColorMap = _.fromPairs(_.zip(taxVars, nums));
            angular.forEach(genes, function (gene, i) {
              gene._source._color = cvCtrl.taxColorMap[gene._source[cvCtrl.highlight]];
            });
          }
        }

        $scope.$watchCollection(angular.bind(cvCtrl.contig, function () {
          return cvCtrl.contig.genes;
        }), function (genes) {
          if (genes && genes.length > 0) {
            var DATA = genes;
            var CONTIGLENGTH = parseInt(cvCtrl.contig._source['length']);
            var HIGHLIGHTGENEID = cvCtrl.geneid;

            var coverageAverageSpan = 10;
            var coverageHeight = 40;
            var xaxisw = 10000;
            var axisoverextension = 0;
            var yspace = cvCtrl.displayCoverage ? 50 + coverageHeight : 40;
            var breakwidth = 2;
            var rows = Math.ceil(d3.max(DATA.map(
              function (d) {
                return d.start;
              }
            )) / xaxisw) + 1;
            if (CONTIGLENGTH === "0") {
              CONTIGLENGTH = xaxisw * rows;
            }
            var margin = {
              top: cvCtrl.displayCoverage ? 20 + coverageHeight : 5,
              right: 10,
              bottom: 15,
              left: cvCtrl.displayCoverage ? 50 : 30
            };
            var width = 860;
            var height = rows * yspace;
            var scale = 12;
            var axisytop = 26;
            var ytop = 0;
            var ypad = 10.5;
            var h = 13;
            var tipw = 4;
            var tipw2 = 4;
            var minGeneLenForLabel = 200;

            var svg = d3.select("svg")
              .attr("width", width + margin.left + margin.right)
              .attr("height", height + margin.top + margin.bottom)
              .attr("class", "contig-viewer")
              .append("g")
              .attr("transform", "translate(" + margin.left + "," + margin.top + ")");


            svg.append("text")
              .attr("x", -500)
              .attr("y", -500)
              .attr("class", "genelabel measure");

            var gene = svg.selectAll("polyline")
              .data(DATA).enter()
              .append("g").attr("class", function (d) {
                return d.geneid === HIGHLIGHTGENEID ? "gene highlight" : "gene";
              });
            var geneshape1 = gene.append("polyline")
              .attr("points", function (gene) {
                var x1 = (gene.start % xaxisw) / scale;
                var x2 = x1 + (gene.stop - gene.start) / scale;
                var y = ypad + yspace * Math.floor(gene.start / xaxisw) + ytop;
                var rightlimit = xaxisw / scale;
                var points = "";
                var c = ",", s = " ";
                var p1 = "", p2 = "", p3 = "", p4 = "", p5 = "", p6 = "";
                if (gene.strand === "+") {
                  if (x2 <= rightlimit) {
                    p1 = x1 + c + y;
                    p2 = (x2 - tipw) + c + y;
                    p3 = x2 + c + (y + (h / 2));
                    p4 = (x2 - tipw) + c + (y + h);
                    p5 = x1 + c + (y + h);
                    p6 = x1 + tipw2 + c + (y + h / 2);
                  } else {
                    p1 = x1 + c + y;
                    p2 = rightlimit + c + y;
                    p3 = p2;
                    p4 = rightlimit + c + (y + h);
                    p5 = x1 + c + (y + h);
                    p6 = x1 + tipw2 + c + (y + h / 2);
                  }
                } else {
                  if (x2 <= rightlimit) {
                    p1 = (x1 + tipw) + c + y;
                    p2 = x2 + c + y;
                    p3 = x2 - tipw2 + c + (y + h / 2);
                    p4 = x2 + c + (y + h);
                    p5 = (x1 + tipw) + c + (y + h);
                    p6 = x1 + c + (y + h / 2);
                  } else {
                    p1 = (x1 + tipw) + c + y;
                    p2 = rightlimit + c + y;
                    p3 = rightlimit + c + (y + h);
                    p4 = (x1 + tipw) + c + (y + h);
                    p5 = x1 + c + (y + h / 2);
                    p6 = p5;
                  }
                }

                points = p1 + s + p2 + s + p3 + s + p4 + s + p5 + s + p6 + s + p1;
                return points;
              })
              .attr("vector-effect", "non-scaling-stroke")
              .attr("class", function (gene) {
                return "fill-color-" + gene._color;
              });
            var geneshape2 = gene.append("polyline")
              .attr("points", function (gene) {
                var x1 = (gene.start % xaxisw) / scale;
                var x2 = x1 + (gene.stop - gene.start) / scale;
                var y = ypad + yspace * Math.floor(gene.start / xaxisw) + ytop;
                var rightlimit = xaxisw / scale;
                var points = "";
                var c = ",", s = " ";
                var p1 = "", p2 = "", p3 = "", p4 = "", p5 = "", p6 = "";
                if (gene.strand === "+") {
                  if (x2 > rightlimit) {
                    p1 = 0 + c + (y + yspace);
                    p2 = (x2 - rightlimit - tipw) + c + (y + yspace);
                    p3 = x2 - rightlimit + c + (y + yspace + (h / 2));
                    p4 = (x2 - rightlimit - tipw) + c + (y + yspace + h);
                    p5 = 0 + c + (y + yspace + h);
                    p6 = p5;
                  }
                } else {
                  if (x2 > rightlimit) {
                    p1 = 0 + c + (y + yspace + h);
                    p2 = 0 + c + (y + yspace);
                    p3 = p2;
                    p4 = x2 - rightlimit + c + (y + yspace);
                    p5 = x2 - rightlimit - tipw2 + c + (y + yspace + h / 2);
                    p6 = x2 - rightlimit + c + (y + yspace + h);
                  }
                }

                points = p1 + s + p2 + s + p3 + s + p4 + s + p5 + s + p6 + s + p1;
                return points;
              })
              .attr("vector-effect", "non-scaling-stroke")
              .attr("class", function (gene) {
                return "fill-color-" + gene._color;
              });
            // var title = gene.append("info").text(function (d) {
            //   return d.geneid + "\n" + d.salltitles + "\n" + d.start + " - " + d.stop;
            // });
            gene.on("click", function (d) {
              $scope.$emit('ContigViewer:selectGene', d);
            });
            gene.attr("style", function (d) {
              var x = ((d.start % xaxisw) / scale) + ((d.stop - d.start) / scale) / 2;
              var y = ypad + yspace * Math.floor(d.start / xaxisw) + ytop + h;
              return "transform-origin: " + x + "px " + y + "px";
            });
            var geneLabel = gene.append("text")
              .attr("class", function (gene) {
                return "genelabel text-color-" + gene._color;
              })
              .attr("x", function (gene) {
                return ((gene.start % xaxisw) / scale) + tipw + 2;
              })
              .attr("y", function (gene) {
                return ypad + yspace * Math.floor(gene.start / xaxisw) + ytop + h - 3;
              })
              .text(function (gene) {
                var geneWidth = (gene.stop - gene.start) / xaxisw * width;
                var labelContent = gene.salltitles;
                if (!labelContent) {
                  labelContent = "";
                }
                var label = "";
                svg.selectAll("text.genelabel.measure").text(labelContent);
                var geneLabelWidth = svg.selectAll("text.genelabel.measure").node().getComputedTextLength();
                var charWidth = geneLabelWidth / labelContent.length;
                label = labelContent.substr(0,
                  Math.max(0,
                    Math.floor(
                      Math.min(
                        geneWidth / charWidth,
                        (width - (gene.start % xaxisw) / scale - 20) / charWidth
                      )
                    ) - 2
                  ));
                cvCtrl.loading = false;
                return (gene.stop - gene.start > minGeneLenForLabel) ? label : "";
              });

            if (cvCtrl.displayCoverage) {
              // coverage plot
              var covX = _.range(0, (CONTIGLENGTH / scale + (2 * coverageAverageSpan)), coverageAverageSpan / scale);
              var covY = new Array(covX.length).fill(0);
              if (cvCtrl.coverageName === cvCtrl.covOptionAllSamples) {
                angular.forEach(cvCtrl.contig._source['coverage'], function (covDataEntry) {
                  angular.forEach(covDataEntry['values'], function (covVal, j) {
                    covY[j] += covVal;
                  });
                });
              } else {
                angular.forEach(cvCtrl.contig._source['coverage'], function (covDataEntry) {
                  if (covDataEntry['name'] === cvCtrl.coverageName) {
                    covY = covDataEntry['values'];
                  }
                });
              }
              covX = _.slice(covX, 0, covY.length);
              var maxCovY = _.max(covY);
              var covData = _.zip(covX, covY);
              var covDataChunkSize = xaxisw / coverageAverageSpan;

              var coveragePlot = d3.select("svg")
                .append("g")
                .attr("class", "coverage-plot")
                .attr("transform", "translate(" + margin.left + "," + (margin.top - 35) + ")");
              coveragePlot.append("text")
                .attr("class", "coverage-plot-info")
                .attr("transform", "translate(0,-10)")
                .attr("text-anchor", "end")
                .text('cov');

              var covYScale = d3.scale.linear().range([0, coverageHeight]).domain([maxCovY, 0]).nice();
              var covArea = d3.svg.area()
                .x(function (d) {
                  return d[0];
                })
                .y0(covYScale(0))
                .y1(function (d) {
                  return covYScale(d[1]);
                });
              var nextCovDataSlice;
            }

            for (var i = 0; i < rows; i++) {
              // x axis rendering
              if (xaxisw * i <= CONTIGLENGTH) {
                var axisScale = d3.scale.linear()
                  .domain([i * xaxisw, (xaxisw * (i + 1) > CONTIGLENGTH) ? CONTIGLENGTH : (i + 1) * xaxisw + (xaxisw * axisoverextension)])
                  .range([0, (xaxisw * (i + 1) > CONTIGLENGTH) ? CONTIGLENGTH % xaxisw / scale : (xaxisw / scale) * (1 + axisoverextension)]);
                var tickCount = (xaxisw * (i + 1) > CONTIGLENGTH) ? CONTIGLENGTH % xaxisw / (xaxisw / 10) : 11;
                var xAxis = d3.svg.axis().ticks(tickCount).tickSize(4).scale(axisScale).tickFormat(function (d, i) {
                  return d / 1000 + 'k';
                });
                svg.append("g").attr("class", "axis")
                  .attr("transform", "translate(0," + (i * yspace + axisytop) + ")")
                  .call(xAxis);

                if (cvCtrl.displayCoverage) {
                  // coverage plot
                  nextCovDataSlice = _.slice(covData, i * covDataChunkSize, (i + 1) * covDataChunkSize);
                  coveragePlot.append("path")
                    .attr("transform", "translate(" + (-i * xaxisw / scale) + "," + (i * yspace) + ")")
                    .attr("d", covArea(nextCovDataSlice));

                  var covAxis = d3.svg.axis().scale(covYScale).ticks(3).tickSize(4);
                  covAxis.orient('left');
                  svg.append("g")
                    .attr("transform", "translate(-1," + ((i * yspace) - 35) + ")")
                    .attr("class", "axis").call(covAxis);
                }
              }

            }

          }
        });

        function downloadContigGbk() {
          cvCtrl.downloadFilename = cvCtrl.dataset.name + "__" + cvCtrl.contigid;
          var sequence = "";
          ElasticSearch.getContig(cvCtrl.dataset.name, cvCtrl.contigid, true, false)
            .then(function (resp) {
              sequence = resp.hits.hits[0]._source['nucleotide'].toLowerCase();
              ElasticSearch.getContigsGenes(cvCtrl.dataset.name, [cvCtrl.contigid],
                _.concat(['geneid', 'salltitles', 'start', 'stop', 'strand', 'amino', 'partial'], cvCtrl.highlight))
                .then(function (respGenes) {
                  var genes = respGenes.hits.hits;
                  genes.sort(function (a, b) {
                    return a._source['start'] - b._source['start'];
                  });


                  //TODO: VirSorter
                  var out = 'LOCUS       ';
                  out += padR(genes[0]._source['geneid'].substring(0, Math.min(10, genes[0]._source['geneid'].indexOf('_'))), 10);
                  out += padL(resp.hits.hits[0]._source['length'].toString(), 18);
                  out += ' bp    DNA     linear   ENV ';
                  var m = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
                  var date = new Date();
                  out += '' + padL(date.getDate().toString(), 2, '0') + '-' + m[date.getMonth()] + '-' + date.getFullYear();
                  out += '\n';
                  out += 'DEFINITION  Contig ' + cvCtrl.contigid + ' of dataset ' + cvCtrl.dataset.name + '\n';
                  out += 'ACCESSION   ';
                  out += genes[0]._source['geneid'].substring(0, genes[0]._source['geneid'].indexOf('_'));
                  out += '_' + cvCtrl.contigid + '\n';
                  //TODO: SOURCE?
                  out += 'FEATURES             Location/Qualifiers\n';
                  out += '     source          1..' + resp.hits.hits[0]._source['length'] + '\n';
                  out += '                     /db_xref="' + cvCtrl.dataset.name + '"\n';
                  out += '                     /db_xref="' + cvCtrl.contigid + '"\n';
                  angular.forEach(genes, function (gene, i) {
                    out += '     CDS             ';
                    if (gene._source['strand'] === '-') {
                      out += 'complement(';
                    }
                    out += '' + gene._source['start'] + '..' + gene._source['stop'];
                    if (gene._source['strand'] === '-') {
                      out += ')';
                    }
                    out += '\n';
                    out += formatFeature('/db_xref="' + gene._source['geneid'] + '"');
                    out += formatFeature('/product="' + gene._source['salltitles'] + '"');
                    if ('partial' in gene._source && gene._source['partial'] !== '') {
                      var note = '';
                      if (gene._source['partial'] === 'yes') {
                        note = 'incomplete';
                      } else if (gene._source['partial'] === 'no') {
                        note = 'complete';
                      }
                      out += formatFeature('/note="' + note + '"');
                    }
                    out += formatFeature('/translation="' + gene._source['amino'].replace('*', '') + '"');
                  });


                  out += 'ORIGIN\n';
                  for (var i = 1; i < sequence.length; i += 60) {
                    var num = i.toString();
                    for (var k = 9; k > num.length; k--) {
                      out += ' ';
                    }
                    out += num;
                    out += ' ';
                    for (var j = 0; j < 60; j += 10) {
                      out += sequence.slice(i - 1 + j, i + j + 9);
                      out += ' ';
                    }
                    out += '\n';
                  }
                  out += '//\n';

                  var downloadBlob = new Blob([out], {type: 'text/plain'});
                  cvCtrl.downloadUrl = (window.URL || window.webkitURL).createObjectURL(downloadBlob);
                  $timeout(function () {
                    document.getElementById('download-contig-gbk').click();
                    (window.URL || window.webkitURL).revokeObjectURL(downloadBlob);
                  }, 500);


                }, function (err) {
                  console.trace(err.message);
                });
            }, function (err) {
              console.trace(err.message);
            })
            .finally(function () {
            });
        }

        function formatFeature(content) {
          var o = '';
          for (var i = 0; i < content.length; i += 58) {
            o += '                     ';
            o += content.slice(i, i + 58);
            o += '\n';
          }
          return o;
        }

        function padL(content, length, char) {
          var o = '';
          for (var k = length; k > content.length; k--) {
            if (char) {
              o += char;
            } else {
              o += ' ';
            }
          }
          o += content;
          return o;
        }

        function padR(content, length) {
          var o = content;
          for (var k = length; k > content.length; k--) {
            o += ' ';
          }
          return o;
        }

        function downloadContigFaa() {
          cvCtrl.downloadFilename = cvCtrl.dataset.name + "__" + cvCtrl.contigid;
          var sequence = "";
          ElasticSearch.getContig(cvCtrl.dataset.name, cvCtrl.contigid, true, false)
            .then(function (resp) {
              sequence = resp.hits.hits[0]._source['nucleotide'].toLowerCase();
              ElasticSearch.getContigsGenes(cvCtrl.dataset.name, [cvCtrl.contigid],
                _.concat(['geneid', 'salltitles', 'amino'], cvCtrl.highlight))
                .then(function (respGenes) {
                  var genes = respGenes.hits.hits;
                  genes.sort(function (a, b) {
                    return a._source['start'] - b._source['start'];
                  });

                  var out = "";
                  angular.forEach(genes, function (gene, i) {
                    out += '>';
                    out += gene._source['geneid'];
                    out += ' - ';
                    out += gene._source['salltitles'];
                    out += '\n';
                    out += gene._source['amino'];
                    out += '\n';
                  });

                  var downloadBlob = new Blob([out], {type: 'text/plain'});
                  cvCtrl.downloadUrl = (window.URL || window.webkitURL).createObjectURL(downloadBlob);
                  $timeout(function () {
                    document.getElementById('download-contig-faa').click();
                    (window.URL || window.webkitURL).revokeObjectURL(downloadBlob);
                  }, 500);


                }, function (err) {
                  console.trace(err.message);
                });
            }, function (err) {
              console.trace(err.message);
            })
            .finally(function () {
            });
        }

        fetch();
      }
    };
  }

})();

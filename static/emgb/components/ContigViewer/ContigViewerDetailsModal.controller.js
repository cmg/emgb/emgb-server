/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.ContigViewer')
    .controller('ContigViewerDetailsModalCtrl', ContigViewerDetailsModalCtrl);

  function ContigViewerDetailsModalCtrl($scope, $uibModalInstance, $timeout, gene, pgtbl, dataset) {
    var cvDetailsModalCtrl = this;

    $scope.gene = {_source: gene};
    $scope.pgTbl = pgtbl;
    $scope.dataset = dataset;
    $scope.contextIsContigViewer = true;

    cvDetailsModalCtrl.close = close;

    function close() {
      $uibModalInstance.dismiss('close');
    }
  }
})();

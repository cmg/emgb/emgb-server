/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.ContigViewer')
    .controller('ContigViewerModalCtrl', ContigViewerModalCtrl);

  function ContigViewerModalCtrl($scope, $uibModal, $uibModalInstance, $timeout, geneid, contigid, dataset, pgTbl) {
    var cvModalCtrl = this;

    cvModalCtrl.geneid = geneid;
    cvModalCtrl.contigid = contigid;
    cvModalCtrl.dataset = dataset;
    cvModalCtrl.pgTbl = pgTbl;

    cvModalCtrl.close = close;

    $scope.$on('ContigViewer:selectGene', function (event, gene) {
      event.stopPropagation && event.stopPropagation();
      $timeout(function () {
        $uibModal.open({
          animation: false,
          backdrop: true,
          templateUrl: 'components/ContigViewer/contig-viewer.details.modal.html',
          controller: 'ContigViewerDetailsModalCtrl',
          controllerAs: 'cvDetailsModalCtrl',
          windowClass: 'gene-details-modal',
          resolve: {
            gene: function () {
              return gene;
            },
            pgtbl: function () {
              return cvModalCtrl.pgTbl;
            },
            dataset: function () {
              return cvModalCtrl.dataset;
            }
          }
        });
      }, 0);
    });

    function close() {
      $uibModalInstance.dismiss('close');
    }
  }
})();

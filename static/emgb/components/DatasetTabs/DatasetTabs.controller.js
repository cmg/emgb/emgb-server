/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.DatasetTabs')
    .controller('DatasetTabs', DatasetTabs);

  function DatasetTabs(ElasticSearch, $rootScope, $scope, $timeout, $location, FilterBox, BLAST_SERVICE_URL, TARGETDB_SERVICE_URL, METADATA_FILE, MODE, DATASETS_PER_PAGE, $http) {
    var datasetTabs = this;

    datasetTabs.availableDatasetNames = [];
    datasetTabs.datasetGeneCounts = [];
    datasetTabs.datasetBinCounts = [];
    datasetTabs.datasetNonPartialGeneCounts = [];
    datasetTabs.datasetContigStats = [];
    datasetTabs.overallGeneCount = 0;
    datasetTabs.overallMapping = {};
    datasetTabs.loadingAvailableDatasetNames = false;
    datasetTabs.loadingGeneCounts = false;
    datasetTabs.loadingFullGenes = false;
    datasetTabs.loadingContigStats = false;
    datasetTabs.loadingExtendedAnnotations = false;
    datasetTabs.selectedDatasets = [];
    datasetTabs.activeTabIndex = 0;
    datasetTabs.cartCount = 0;
    datasetTabs.lookupLoading = false;
    datasetTabs.inputForLookup = "";
    datasetTabs.lookupSuccessful = true;
    datasetTabs.blastLoading = false;
    datasetTabs.inputForBlast = "";
    datasetTabs.blastHits = [];
    datasetTabs.blastSuccessful = true;
    datasetTabs.blastHasError = false;
    datasetTabs.blastError = "";
    datasetTabs.blastUnavailable = false;
    datasetTabs.systemInfo = {};
    datasetTabs.blastEndpointQueryFinished = false;
    datasetTabs.datasetMetadata = {};
    datasetTabs.hasDatasetMetadata = false;
    datasetTabs.datasetSizePercentages = [];
    datasetTabs.mode = MODE;
    datasetTabs.targetDbHits = [];
    datasetTabs.targetDbSuccessful = true;
    datasetTabs.targetDbHasError = false;
    datasetTabs.targetDbError = "";
    datasetTabs.targetDbLoading = false;
    datasetTabs.inputForTargetDb = "";
    datasetTabs.targetDbUnavailable = false;
    datasetTabs.targetDbSystemInfo = {};
    datasetTabs.targetDbEndpointQueryFinished = false;
    datasetTabs.blastMaxResults = "25";
    datasetTabs.currentBlastMaxResults = "25";  // for display only
    datasetTabs.currentTargetdbMaxResults = "25";  // for display only
    datasetTabs.filterCloneModeOn = false;
    datasetTabs.filterCloneModeExcludedDatasetId = -1;
    datasetTabs.binCountMaxContamination = 0.1;
    datasetTabs.binCountMinCompleteness = 0.7;
    datasetTabs.datasetsPerPage = DATASETS_PER_PAGE;
    datasetTabs.activeDatasetsPage = 1;

    datasetTabs.addTab = addTab;
    datasetTabs.removeTab = removeTab;
    datasetTabs.tabSelected = tabSelected;
    datasetTabs.tabLabelChanged = tabLabelChanged;
    datasetTabs.loadAvailableDatasetNames = loadAvailableDatasetNames;
    datasetTabs.lookupGeneIdOrAmino = lookupGeneIdOrAmino;
    datasetTabs.blastSequence = blastSequence;
    datasetTabs.targetDbSearchSequence = targetDbSearchSequence;
    datasetTabs.openBlastHitInNewTab = openBlastHitInNewTab;
    datasetTabs.resetBlastForm = resetBlastForm;
    datasetTabs.resetTargetDbForm = resetTargetDbForm;
    datasetTabs.requestFiltersFromTab = requestFiltersFromTab;
    datasetTabs.pageForward = pageForward;
    datasetTabs.pageBack = pageBack;
    datasetTabs.goToPage = goToPage;
    datasetTabs.getPageRange = getPageRange;
    datasetTabs.getTotalPages = getTotalPages;

    var idCounter = 10;

    $scope.$on('CompareTab:dataset:relay', function (event, dataset, boolQuery) { // tab group is notified
      event.stopPropagation && event.stopPropagation();
      // informs compare tab (downwards):
      $scope.$broadcast('CompareTab:dataset', dataset, boolQuery);
    });

    $scope.$on('Cart:add:relay', function (event, dataset, geneid) { // tab group is notified
      event.stopPropagation && event.stopPropagation();
      // informs cart tab (downwards):
      $scope.$broadcast('Cart:add', dataset, geneid);
    });
    $scope.$on('Cart:sendCopy:relay', function (event, data) { // tab group is notified
      event.stopPropagation && event.stopPropagation();
      $scope.$broadcast('Cart:sendCopy', data);
    });

    $scope.$on('Cart:requestCopy:relay', function (event) { // tab group is notified
      event.stopPropagation && event.stopPropagation();
      // informs cart tab (downwards):
      $scope.$broadcast('Cart:requestCopy');
    });

    $scope.$on('Cart:count', function (event, count) {
      event.stopPropagation && event.stopPropagation();
      datasetTabs.cartCount = count;
    });

    $scope.$on('DatasetTabs:requestTabCount', function (event) {
      event.stopPropagation && event.stopPropagation();
      $scope.$broadcast('DatasetTabs:tabCount', datasetTabs.selectedDatasets.length);
    });

    $scope.$on('DatasetTabs:filterCloneMode', function (event, value, excludedDatasetId) {
      event.stopPropagation && event.stopPropagation();
      datasetTabs.filterCloneModeOn = value;
      datasetTabs.filterCloneModeExcludedDatasetId = excludedDatasetId;
    });

    $scope.$on('DatasetTab:filters:relay', function (event, filters) {
      event.stopPropagation && event.stopPropagation();
      $scope.$broadcast('DatasetTab:filters', filters);
    });

    function tabSelected(tabLabel) {
      $timeout(function () {
        $scope.$broadcast('DatasetTabs:changed', tabLabel);
      });
    }

    function tabLabelChanged(dataset) {
      $scope.$broadcast('CompareTab:tabLabel', dataset);
    }

    function loadDatasetGeneCounts(datasets) {
      datasetTabs.loadingGeneCounts = true;
      ElasticSearch.getDatasetGeneCounts(datasets, false)
        .then(function (resp) {
          datasetTabs.overallGeneCount = 0;
          var highestGeneCount = 0;
          angular.forEach(resp.responses, function (response) {
            datasetTabs.datasetGeneCounts.push(response.hits.total);
            datasetTabs.overallGeneCount += response.hits.total;
            if (highestGeneCount < response.hits.total) {
              highestGeneCount = response.hits.total;
            }
          });
          angular.forEach(datasetTabs.datasetGeneCounts, function (count) {
            datasetTabs.datasetSizePercentages.push(Math.round((count / highestGeneCount) * 100));
          });
        }, function (err) {
          console.trace(err.message);
        })
        .finally(function () {
          datasetTabs.loadingGeneCounts = false;
          loadDatasetNonPartialGeneCounts(datasetTabs.availableDatasetNames);
        });
    }

    function loadDatasetBinCounts(datasets, minCompleteness, maxContamination) {
      datasetTabs.loadingBinCounts = true;
      ElasticSearch.getDatasetBinCounts(datasets, minCompleteness, maxContamination)
        .then(function (resp) {
          angular.forEach(resp.responses, function (response) {
            datasetTabs.datasetBinCounts.push(response.hits.total);
          });
        }, function (err) {
          console.trace(err.message);
        })
        .finally(function () {
          datasetTabs.loadingBinCounts = false;
          $rootScope.stopSpinner();
        });
    }

    function loadDatasetContigStats(datasets) {
      datasetTabs.loadingContigStats = true;
      ElasticSearch.getDatasetContigStats(datasets)
        .then(function (resp) {
          angular.forEach(resp.responses, function (response) {
            datasetTabs.datasetContigStats.push({
              'numberOfContigs': response.hits.total,
              'assemblySize': response.aggregations.assemblySize.value,
              'n50': Nx0(0.5, response.aggregations.lengths.buckets, response.aggregations.assemblySize.value),
              'n90': Nx0(0.9, response.aggregations.lengths.buckets, response.aggregations.assemblySize.value)
            });
          });
        }, function (err) {
          console.trace(err.message);
        })
        .finally(function () {
          datasetTabs.loadingContigStats = false;
          loadDatasetBinCounts(datasets, datasetTabs.binCountMinCompleteness, datasetTabs.binCountMaxContamination);
        });
    }

    function Nx0(n, buckets, assemblySize) {
      var sum = 0;
      var nx0 = 0;
      angular.forEach(buckets, function (bucket) {
        sum += bucket.key * bucket.doc_count;
        if (sum < (assemblySize * n)) {
          nx0 = bucket.key;
        }
      });
      return nx0;
    }

    function loadDatasetNonPartialGeneCounts(datasets) {
      datasetTabs.loadingFullGenes = true;
      ElasticSearch.getDatasetGeneCounts(datasets, true)
        .then(function (resp) {
          angular.forEach(resp.responses, function (response) {
            datasetTabs.datasetNonPartialGeneCounts.push(response.hits.total);
          });
        }, function (err) {
          console.trace(err.message);
        })
        .finally(function () {
          datasetTabs.loadingFullGenes = false;
          loadOverallMapping();
        });
    }

    function loadOverallMapping() {
      datasetTabs.loadingExtendedAnnotations = true;
      ElasticSearch.getMapping()
        .then(function (resp) {
          datasetTabs.overallMapping = resp;
        }, function (err) {
          console.trace(err.message);
        })
        .finally(function () {
          datasetTabs.loadingExtendedAnnotations = false;
          loadDatasetContigStats(datasetTabs.availableDatasetNames);
        });
    }

    function getGeneCountForDataset(datasetName) {
      return datasetTabs.datasetGeneCounts[datasetTabs.availableDatasetNames.indexOf(datasetName)];
    }

    function loadDatasetMetadata() {
      datasetTabs.loadingExtendedAnnotations = true;
      $rootScope.startSpinner();
      $http(
        {
          method: 'GET',
          url: METADATA_FILE,
          headers: {
            'Accept': 'application/json'
          },
          timeout: 3000
        })
        .then(function (resp) {
          datasetTabs.datasetMetadata = resp.data;
          if (Object.keys(datasetTabs.datasetMetadata).length > 0) {
            datasetTabs.hasDatasetMetadata = true;
          }
        }, function (err) {
          console.trace(err);
        })
        .finally(function () {
          datasetTabs.loadingExtendedAnnotations = false;
          $rootScope.stopSpinner();
        });
    }

    function loadAvailableDatasetNames() {
      $rootScope.startSpinner();
      datasetTabs.loadingAvailableDatasetNames = true;
      ElasticSearch.getAliases()
        .then(function (resp) {
          datasetTabs.availableDatasetNames = Object.keys(resp);
          datasetTabs.availableDatasetNames.sort(function (a, b) {
            return a.localeCompare(b);
          });
          openGeneIdFromURLParam();
          if (datasetTabs.availableDatasetNames.length > 0) {
            loadDatasetGeneCounts(datasetTabs.availableDatasetNames);
            loadDatasetMetadata();
          } else {
            datasetTabs.overallGeneCount = 0;
            datasetTabs.loadingAvailableDatasetNames = false;
            $rootScope.stopSpinner();
          }
        }, function (err) {
          datasetTabs.loadingAvailableDatasetNames = false;
          datasetTabs.overallGeneCount = 0;
          $rootScope.stopSpinner();
          console.trace(err.message);
        })
        .finally(function () {
          datasetTabs.loadingAvailableDatasetNames = false;
          // addTab('matis_hrifla1145_bac'); //TODO: remove (auto)
        });
    }

    loadAvailableDatasetNames();

    function addTab(datasetName) {
      var newDatasetTab = {id: idCounter, name: datasetName, total: getGeneCountForDataset(datasetName)};
      datasetTabs.selectedDatasets.push(newDatasetTab);
      $timeout(function () {
        datasetTabs.activeTabIndex = newDatasetTab.id;
      }, 0);
      idCounter++;
      $scope.$broadcast('DatasetTabs:tabCount', datasetTabs.selectedDatasets.length);
      return newDatasetTab;
    }

    function removeTab(dataset) {
      datasetTabs.selectedDatasets = datasetTabs.selectedDatasets.filter(function (el) {
        return el.id !== dataset.id;
      });
      if (datasetTabs.selectedDatasets.length === 0) {
        datasetTabs.activeTabIndex = 0;
      }
      $scope.$broadcast('CompareTab:dataset:remove', dataset);
      if (datasetTabs.activeTabIndex === -1) {
        $timeout(function () {
          $scope.$broadcast('DatasetTabs:changed', 'compare-tab');
        });
        if (datasetTabs.selectedDatasets.length < 2) {
          datasetTabs.activeTabIndex = 0;
        }
      }
      datasetTabs.filterCloneModeOn = false;
      $scope.$broadcast('DatasetTabs:tabCount', datasetTabs.selectedDatasets.length);
    }

    function lookupGeneIdOrAmino() {
      $rootScope.startSpinner();
      datasetTabs.lookupLoading = true;
      ElasticSearch.searchAllForGeneIdOrAmino(datasetTabs.inputForLookup)
        .then(function (resp) {
          if (resp.hits.hits.length > 0) {
            angular.forEach(resp.hits.hits, function (hit) {
              var dataset = hit._index;
              var geneId = hit._source['geneid'];
              var tab = addTab(dataset);
              tab.initialFilter = new FilterBox('Direct lookup');
              tab.initialFilter.addFilter('geneid', geneId);
            });
            datasetTabs.lookupSuccessful = true;
          } else {
            datasetTabs.lookupSuccessful = false;
          }
        }, function (err) {
          console.trace(err.message);
        })
        .finally(function () {
          datasetTabs.lookupLoading = false;
          $rootScope.stopSpinner();
        });
    }

    function blastSequence(tool, sequence) {
      $rootScope.startSpinner();
      datasetTabs.blastSuccessful = true;
      datasetTabs.blastHasError = false;
      datasetTabs.blastLoading = true;
      $http(
        {
          method: 'POST',
          url: BLAST_SERVICE_URL + 'run_blast',
          headers: {
            'Accept': 'application/json'
          },
          data: {
            seq_type: tool,
            query: sequence,
            return_alignment: true,
            limit: datasetTabs.blastMaxResults
          }
        })
        .then(function (resp) {
          datasetTabs.blastUnavailable = false;
          datasetTabs.blastHits = resp.data.hits;
          datasetTabs.blastSuccessful = datasetTabs.blastHits.length > 0;
          enhanceBlastHits();
          datasetTabs.currentBlastMaxResults = datasetTabs.blastMaxResults;
        }, function (err) {
          datasetTabs.blastHasError = true;
          datasetTabs.blastUnavailable = false;
          if (err.status === 422) {
            datasetTabs.blastError = "Invalid input. " + err.data.error;
          } else if (err.status === 400) {
            datasetTabs.blastError = "Invalid input.";
          } else {
            datasetTabs.blastHasError = false;
            datasetTabs.blastUnavailable = true;
          }
        })
        .finally(function () {
          datasetTabs.blastLoading = false;
          $rootScope.stopSpinner();
        });
    }

    function targetDbSearchSequence(sequence) {
      $rootScope.startSpinner();
      datasetTabs.targetDbSuccessful = true;
      datasetTabs.targetDbHasError = false;
      datasetTabs.targetDbLoading = true;
      $http(
        {
          method: 'POST',
          url: TARGETDB_SERVICE_URL + 'run_blast',
          headers: {
            'Accept': 'application/json'
          },
          data: {
            seq_type: 'p',
            query: sequence,
            return_alignment: true,
            limit: datasetTabs.blastMaxResults
          }
        })
        .then(function (resp) {
          datasetTabs.targetDbUnavailable = false;
          datasetTabs.targetDbHits = resp.data.hits;
          datasetTabs.targetDbSuccessful = datasetTabs.targetDbHits.length > 0;
          datasetTabs.currentTargetdbMaxResults = datasetTabs.blastMaxResults;
        }, function (err) {
          datasetTabs.targetDbHasError = true;
          datasetTabs.targetDbUnavailable = false;
          if (err.status === 422) {
            datasetTabs.targetDbError = "Invalid input. " + err.data.error;
          } else if (err.status === 400) {
            datasetTabs.targetDbError = "Invalid input.";
          } else {
            datasetTabs.targetDbHasError = false;
            datasetTabs.targetDbUnavailable = true;
          }
        })
        .finally(function () {
          datasetTabs.targetDbLoading = false;
          $rootScope.stopSpinner();
        });
    }

    function enhanceBlastHits() {
      var blastHitsIds = [];
      angular.forEach(datasetTabs.blastHits, function (hit) {
        blastHitsIds.push(hit.id);
      });
      $rootScope.startSpinner();
      ElasticSearch.searchAllForGeneIds(blastHitsIds, ['geneid', 'salltitles', 'partial'])
        .then(function (resp) {
          angular.forEach(resp.hits.hits, function (hit) {
            angular.forEach(datasetTabs.blastHits, function (blastHit) {
              if (blastHit.id === hit._source['geneid']) {
                blastHit.dataset = hit._index;
                blastHit.salltitles = hit._source['salltitles'];
                blastHit.partial = hit._source['partial'];
              }
            });
          });
        }, function (err) {
          console.trace(err.message);
        })
        .finally(function () {
          datasetTabs.blastLoading = false;
          $rootScope.stopSpinner();
        });
    }

    function openBlastHitInNewTab(blastHit) {
      var tab = addTab(blastHit.dataset);
      tab.initialFilter = new FilterBox('BLAST+');
      tab.initialFilter.addFilter('geneid', blastHit.id);
    }

    function resetBlastForm() {
      datasetTabs.inputForBlast = '';
      datasetTabs.blastSuccessful = true;
      datasetTabs.blastHits = [];
    }

    function resetTargetDbForm() {
      datasetTabs.inputForTargetDb = '';
      datasetTabs.targetDbSuccessful = true;
      datasetTabs.targetDbHits = [];
    }

    function getSystemInfo() {
      $rootScope.startSpinner();
      $http(
        {
          method: 'GET',
          url: BLAST_SERVICE_URL + 'system_info',
          headers: {
            'Accept': 'application/json'
          },
          timeout: 3000
        })
        .then(function (resp) {
          datasetTabs.blastUnavailable = false;
          datasetTabs.systemInfo = resp.data;
        }, function (err) {
          datasetTabs.blastUnavailable = true;
        })
        .finally(function () {
          $rootScope.stopSpinner();
          datasetTabs.blastEndpointQueryFinished = true;
        });
    }

    function getTargetDbSystemInfo() {
      $rootScope.startSpinner();
      $http(
        {
          method: 'GET',
          url: TARGETDB_SERVICE_URL + 'system_info',
          headers: {
            'Accept': 'application/json'
          },
          timeout: 3000
        })
        .then(function (resp) {
          datasetTabs.targetDbUnavailable = false;
          datasetTabs.targetDbSystemInfo = resp.data;
        }, function (err) {
          datasetTabs.targetDbUnavailable = true;
        })
        .finally(function () {
          $rootScope.stopSpinner();
          datasetTabs.targetDbEndpointQueryFinished = true;
        });
    }

    function openGeneIdFromURLParam() {
      var params = $location.search();
      if (params['geneid'] && typeof params['geneid'] === 'string' && params['geneid'].length > 0) {
        datasetTabs.inputForLookup = params['geneid'];
        $location.search('geneid', null);
        lookupGeneIdOrAmino();
      }
    }

    function requestFiltersFromTab(datasetId) {
      $timeout(function () {
        $scope.$broadcast('DatasetTab:requestFilters', datasetId);
      });
    }

    getSystemInfo();
    if (datasetTabs.mode === 'viral') {
      getTargetDbSystemInfo();
    }

    function pageForward() {
      if (datasetTabs.activeDatasetsPage < getTotalPages()) {
        datasetTabs.activeDatasetsPage += 1;
      }
    }

    function pageBack() {
      if (datasetTabs.activeDatasetsPage > 1) {
        datasetTabs.activeDatasetsPage -= 1;
      }
    }

    function goToPage(page) {
      datasetTabs.activeDatasetsPage = page;
    }

    function getPageRange() {
      return _.range(1, getTotalPages() + 1);
    }

    function getTotalPages() {
      return Math.ceil(datasetTabs.availableDatasetNames.length / datasetTabs.datasetsPerPage);
    }

  }

})();

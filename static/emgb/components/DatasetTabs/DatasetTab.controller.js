/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.DatasetTabs')
    .controller('DatasetTab', DatasetTab);

  function DatasetTab($scope, $timeout, Utils) {
    var datasetTab = this;

    datasetTab.phyloTreeVisible = true;
    datasetTab.togglePhyloTree = togglePhyloTree;

    var filters = {};
    var boolQuery = {};

    boolQuery = Utils.generateBoolQuery(filters);
    $scope.$emit('CompareTab:dataset:relay', $scope.dataset, boolQuery);

    $scope.$on('ResultFilters:changed:relay', function (event, filterName, filter) { // tab is notified
      event.stopPropagation && event.stopPropagation();
      putFilter(filterName, filter);
      // informs own tab contents:
      $scope.$broadcast('ResultFilters:changed', boolQuery);
      // propagates notification further up to tab group:
      $scope.$emit('CompareTab:dataset:relay', $scope.dataset, boolQuery);
    });

    $scope.$on('TextQuery:changed:relay', function (event, textQueryString) {
      event.stopPropagation && event.stopPropagation();
      $scope.$broadcast('TextQuery:changed', textQueryString);
    });

    $scope.$on('FilterBoxes:add:relay', function (event, newBox) {
      event.stopPropagation && event.stopPropagation();
      $scope.$broadcast('FilterBoxes:add', newBox);
    });

    $scope.$on('PaginatedTable:openContigPopup:relay', function (event, contigid, geneid) {
      event.stopPropagation && event.stopPropagation();
      $scope.$broadcast('PaginatedTable:openContigPopup', contigid, geneid);
    });

    $scope.$watch(angular.bind(datasetTab, function () {
      return datasetTab.phyloTreeVisible;
    }), function () {
      $timeout(function () {
        $scope.$broadcast('DatasetTab:updateLayout');
      }, 0);
    });

    function putFilter(filterName, filter) {
      filters[filterName] = filter;
      boolQuery = Utils.generateBoolQuery(filters);
    }

    function togglePhyloTree() {
      datasetTab.phyloTreeVisible = !datasetTab.phyloTreeVisible;
      $scope.$broadcast('PhyloTree:toggled', datasetTab.phyloTreeVisible);
    }
  }

})();

/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.CompareTab')
    .controller('CompareTab', CompareTab);

  function CompareTab($scope) {
    var compareTab = this;

    compareTab.datasets = [];

    $scope.$on('CompareTab:dataset', function (event, dataset, boolQuery) {
      var existingAt = -1;
      for (var i = 0; i < compareTab.datasets.length; i++) {
        if (compareTab.datasets[i].id === dataset.id) {
          existingAt = i;
        }
      }
      var newEntry = {name: dataset.name, tabLabel: dataset.tabLabel, id: dataset.id, query: boolQuery, total: dataset.total};
      if (existingAt >= 0) {
        compareTab.datasets[existingAt] = newEntry;
      } else {
        compareTab.datasets.push(newEntry);
      }
    });

    $scope.$on('CompareTab:tabLabel', function (event, dataset) {
      for (var i = 0; i < compareTab.datasets.length; i++) {
        if (compareTab.datasets[i].id === dataset.id) {
          compareTab.datasets[i].tabLabel = dataset.tabLabel;
        }
      }
    });

    $scope.$on('CompareTab:dataset:remove', function (event, dataset) {
      compareTab.datasets = compareTab.datasets.filter(function (el) {
        return el.id !== dataset.id;
      });
    });
  }
})();

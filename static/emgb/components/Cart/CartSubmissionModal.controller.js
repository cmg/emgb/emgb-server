/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.Cart')
    .controller('CartSubmissionModalCtrl', CartSubmissionModalCtrl);

  function CartSubmissionModalCtrl($scope, $rootScope, $uibModal, $uibModalInstance, $http, cart, is3DM, ElasticSearch, $q, WIKI_TARGET_SUBMISSION_URL, WIKI_TARGET_SUBMISSION_ENABLED, WIKI_3DM_SUBMISSION_URL) {
    var csModalCtrl = this;

    csModalCtrl.geneids = [];
    csModalCtrl.targetData = [];
    csModalCtrl.submissionData = [];
    csModalCtrl.loading = false;
    csModalCtrl.submitting = false;
    csModalCtrl.getColumnLabel = cart.getColumnLabel;
    csModalCtrl.mode = cart.mode;
    csModalCtrl.username = "";
    csModalCtrl.submissionCompleted = false;
    csModalCtrl.submissionSuccessful = false;
    csModalCtrl.submissionErrorData = {};
    csModalCtrl.enabled = WIKI_TARGET_SUBMISSION_ENABLED;
    csModalCtrl.is3DM = is3DM;

    csModalCtrl.close = close;
    csModalCtrl.submitToWiki = submitToWiki;
    csModalCtrl.submitTo3DM = submitTo3DM;
    csModalCtrl.updateSubmissionData = updateSubmissionData;


    function extractGeneIds() {
      angular.forEach(cart.data.genes, function (genesOfDataset) {
        angular.forEach(Object.keys(genesOfDataset), function (geneid) {
          if (cart.otherColumns[geneid]) {
            csModalCtrl.geneids.push(geneid);
          }
        });
      });
      csModalCtrl.username = $rootScope.getUsername();
      loadGeneInfo();
    }

    function loadGeneInfo() {
      $rootScope.startSpinner();
      csModalCtrl.loading = true;
      ElasticSearch.searchAllForGeneIds(csModalCtrl.geneids, [])
        .then(function (resp) {
          angular.forEach(resp.hits.hits, function (hit) {
            hit._source._submit = true;
            csModalCtrl.targetData.push(hit._source);
          });
          updateSubmissionData();
        }, function (err) {
          console.trace(err.message);
        })
        .finally(function () {
          csModalCtrl.loading = false;
          $rootScope.stopSpinner();
        });
    }

    function updateSubmissionData() {
      csModalCtrl.submissionData = angular.copy(csModalCtrl.targetData).filter(function (item) {
        return item._submit === true;
      });
      angular.forEach(csModalCtrl.submissionData, function (item) {
        delete item._submit;
      });
    }

    // function submissionDummy(outcome) {
    //   return $q(function (resolve, reject) {
    //     setTimeout(function () {
    //       if (outcome === 'ok') {
    //         resolve({'status': 'Successful (Test)', 'data': 'https://example.com'});
    //       } else {
    //         reject({'status': 'Failed (Test)', 'data': 'this is a test'});
    //       }
    //     }, 1000);
    //   });
    // }

    function submitToWiki() {
      $rootScope.startSpinner();
      csModalCtrl.submitting = true;
      $http(
        {
          method: 'POST',
          url: WIKI_TARGET_SUBMISSION_URL,
          headers: {
            'Accept': 'application/json',
            'Authorization': $rootScope.getAuthHeader()
          },
          data: csModalCtrl.submissionData
        })
      // var promise = submissionDummy('f');
      // promise
        .then(function (resp) {
          csModalCtrl.submissionCompleted = true;
          csModalCtrl.submissionSuccessful = true;
        }, function (err) {
          csModalCtrl.submissionCompleted = true;
          csModalCtrl.submissionSuccessful = false;
          csModalCtrl.submissionErrorData = err;
        })
        .finally(function () {
          $rootScope.stopSpinner();
        });
    }

    function submitTo3DM() {
      $rootScope.startSpinner();
      csModalCtrl.submitting = true;
      $http(
        {
          method: 'POST',
          url: WIKI_3DM_SUBMISSION_URL,
          headers: {
            'Accept': 'application/json',
            'Authorization': $rootScope.getAuthHeader()
          },
          data: csModalCtrl.submissionData
        })
      // var promise = submissionDummy('ok');
      // promise
        .then(function (resp) {
          var tdmResponse = resp.data;
          angular.forEach(csModalCtrl.targetData, function (gene) {
            gene.tdmReferences = [];
            angular.forEach(tdmResponse, function (tdmResponseItem) {
              if (tdmResponseItem['emgb_id'] === gene.geneid) {
                angular.forEach(tdmResponseItem['systems'], function (system) {
                  gene.tdmReferences.push({
                    system: system,
                    protein: tdmResponseItem['tdm_id']
                  });
                });
              }
            });
          });
          csModalCtrl.submissionCompleted = true;
          csModalCtrl.submissionSuccessful = true;
        }, function (err) {
          csModalCtrl.submissionCompleted = true;
          csModalCtrl.submissionSuccessful = false;
          csModalCtrl.submissionErrorData = err;
        })
        .finally(function () {
          $rootScope.stopSpinner();
        });
    }

    function close() {
      $uibModalInstance.dismiss('close');
    }

    extractGeneIds();
  }
})();

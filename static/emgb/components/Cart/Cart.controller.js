/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.Cart')
    .controller('Cart', Cart);

  function Cart($scope, $rootScope, $timeout, $uibModal, ElasticSearch, CART_VERSION, CART_TYPESTRING, CART_LIMIT, $q, PAGINATED_TABLE_COLUMN_LABELS, MODE, WIKI_3DM_SUBMISSION_AVAILABLE) {
    var cart = this;

    cart.data = {};
    cart.data.version = CART_VERSION;
    cart.data.type = CART_TYPESTRING;
    cart.data.name = cart.defaultName;
    cart.data.genes = {};
    cart.count = 0;
    cart.downloadUrl;
    cart.downloadExt = "";
    cart.localStorageKey = 'cart';
    cart.uploadedCart = {};
    cart.uploadedCart.contents = "";
    cart.generatingFile = false;
    cart.fileGenerated = false;
    cart.otherColumns = {};
    cart.defaultName = "cart";
    cart.wiki3dmSubmissionAvailable = WIKI_3DM_SUBMISSION_AVAILABLE;
    cart.mode = MODE;

    cart.exportAsFasta = exportAsFasta;
    cart.exportAsLocalStorageFile = exportAsLocalStorageFile;
    cart.removeFromCart = removeFromCart;
    cart.isEmpty = isEmpty;
    cart.clear = clear;
    cart.getColumnLabel = getColumnLabel;
    cart.openSendToWikiDialog = openSendToWikiDialog;
    cart.make3DMDialogAvailable = make3DMDialogAvailable;
    cart.openSendTo3DMDialog = openSendTo3DMDialog;

    function resetCart() {
      cart.data = {};
      cart.data.version = CART_VERSION;
      cart.data.type = CART_TYPESTRING;
      cart.data.name = cart.defaultName;
      cart.data.genes = {};
    }

    $scope.$on('Cart:add', function (event, dataset, geneid) {
      addToCart(dataset, geneid);
      $scope.$emit('Cart:sendCopy:relay', cart.data);
      updateCount();
    });

    $scope.$on('Cart:requestCopy', function (event) {
      $scope.$emit('Cart:sendCopy:relay', cart.data);
    });

    $scope.$watch(angular.bind(cart, function () {
      return cart.data.name;
    }), function () {
      $timeout(function () {
        saveToLocalStorage();
      }, 0);
    });

    $scope.$watch(angular.bind(cart, function () {
      return cart.uploadedCart.contents;
    }), function () {
      $timeout(function () {
        if (cart.uploadedCart.contents !== "") {
          var storedData = angular.fromJson(cart.uploadedCart.contents);
          if (storedData.version === cart.data.version) {
            cart.data = storedData;
            updateCount();
            saveToLocalStorage();
            fetchDisplayColumns();
            cart.uploadedCart = {};
            cart.uploadedCart.contents = "";
          }
        }
      }, 0);
    });

    function addToCart(dataset, geneid) {
      if (cart.count < CART_LIMIT) {
        if (!cart.data.genes.hasOwnProperty(dataset)) {
          cart.data.genes[dataset] = {};
        }
        cart.data.genes[dataset][geneid] = true;
        saveToLocalStorage();
        fetchDisplayColumns();
      }
    }

    function removeFromCart(dataset, geneid) {
      delete cart.data.genes[dataset][geneid];
      if (Object.keys(cart.data.genes[dataset]).length === 0) {
        delete cart.data.genes[dataset];
      }
      $scope.$emit('Cart:sendCopy:relay', cart.data);
      updateCount();
      saveToLocalStorage();
    }

    function clear() {
      resetCart();
      updateCount();
      saveToLocalStorage();
    }

    function updateCount() {
      var count = 0;
      angular.forEach(cart.data.genes, function (geneIds, dataset) {
        angular.forEach(geneIds, function (geneId) {
          count++;
        });
      });
      cart.count = count;
      $scope.$emit('Cart:count', count);
    }

    function saveToLocalStorage() {
      localStorage.setItem(cart.localStorageKey, angular.toJson(cart.data));
    }

    function loadFromLocalStorage() {
      if (localStorage.getItem(cart.localStorageKey)) {
        var storedData = angular.fromJson(localStorage.getItem(cart.localStorageKey));
        if (storedData.version === cart.data.version) {
          cart.data = storedData;
        } else {
          localStorage.removeItem(cart.localStorageKey);
        }
      }
    }

    loadFromLocalStorage();
    updateCount();
    fetchDisplayColumns();

    function isEmpty() {
      return Object.keys(cart.data.genes).length === 0;
    }

    function getColumnLabel(column) {
      return PAGINATED_TABLE_COLUMN_LABELS[column] || column.charAt(0).toUpperCase() + column.slice(1);
    }

    function exportAsFasta(dataColumnName) {
      $rootScope.startSpinner();
      cart.generatingFile = true;
      var fasta = "";

      function appendToFasta(resp) {
        if (resp) {
          angular.forEach(resp.hits.hits, function (hit) {
            fasta = fasta + '>' + hit['_source']['geneid'] + ' dataset=' + hit['_source']['dataset'] + '; salltitles=' + hit['_source']['salltitles'] + '\n';
            fasta = fasta + hit['_source'][dataColumnName] + '\n';
          });
        }
      }

      var promise = $q.defer();
      promise.resolve();
      promise = promise.promise;
      angular.forEach(cart.data.genes, function (geneIds, dataset) {
        promise = promise
          .then(function (resp) {
            appendToFasta(resp);
            return ElasticSearch.resolveCartDataset(dataset, Object.keys(geneIds), ['geneid', 'salltitles', 'dataset', dataColumnName]);
          }, function (err) {
          });
      });
      promise
        .then(function (resp) {
          appendToFasta(resp);
          cart.fileGenerated = true;
          cart.generatingFile = false;
          $rootScope.stopSpinner();
        });
      promise
        .then(function (res) {
          if (dataColumnName === 'nucleotide') {
            cart.downloadExt = 'fna';
          } else if (dataColumnName === 'amino') {
            cart.downloadExt = 'faa';
          }
          generateFileDownload(fasta);
        });
    }

    //TODO: export as tsv

    function exportAsLocalStorageFile() {
      if (localStorage.getItem(cart.localStorageKey)) {
        cart.downloadExt = 'cart';
        generateFileDownload(localStorage.getItem(cart.localStorageKey));
      }
    }

    function generateFileDownload(contents) {
      var downloadBlob = new Blob([contents], {type: 'text/plain'});
      cart.downloadUrl = (window.URL || window.webkitURL).createObjectURL(downloadBlob);
      $timeout(function () {
        document.getElementById('download-cart').click();
        (window.URL || window.webkitURL).revokeObjectURL(downloadBlob);
      }, 500);
    }

    function fetchDisplayColumns() {
      fetchOtherColumns(['geneid', 'salltitles', 'length', 'amino', 'partial']);
    }

    function fetchOtherColumns(columns) {
      $rootScope.startSpinner();
      cart.otherColumns = {};

      function addtoMap(resp) {
        if (resp) {
          angular.forEach(resp.hits.hits, function (hit) {
            cart.otherColumns[hit['_source']['geneid']] = hit['_source'];
          });
        }
      }

      var promise = $q.defer();
      promise.resolve();
      promise = promise.promise;
      angular.forEach(cart.data.genes, function (geneids, dataset) {
        promise = promise
          .then(function (resp) {
            addtoMap(resp);
            return ElasticSearch.resolveCartDataset(dataset, Object.keys(geneids), columns);
          }, function (err) {
          }).catch(function (err) {
            return;  // Do not abort the promise chain on error. Instead, continue resolving the remaining datasets.
          });
      });
      promise
        .then(function (resp) {
          addtoMap(resp);
        })
        .finally(function () {
          $rootScope.stopSpinner();
        });
    }

    function openSubmissionModal(is3DM) {
      $uibModal.open({
        animation: false,
        backdrop: true, //'static',
        templateUrl: 'components/Cart/submission.modal.html',
        controller: 'CartSubmissionModalCtrl',
        controllerAs: 'csModalCtrl',
        size: 'lg',
        resolve: {
          cart: function () {
            return cart;
          },
          is3DM: function () {
            return is3DM;
          }
        }
      });
    }

    function openSendToWikiDialog() {
      openSubmissionModal(false);
    }

    function make3DMDialogAvailable() {
      cart.wiki3dmSubmissionAvailable = true;
    }

    function openSendTo3DMDialog() {
      openSubmissionModal(true);
    }
  }
})();

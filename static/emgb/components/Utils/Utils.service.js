/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.Utils')
    .service('Utils', Utils);

  function Utils() {
    this.generateBoolQuery = generateBoolQuery;
    this.shorten = shorten;
    this.sortObjectArray = sortObjectArray;
    this.sortObjectArrayByNumber = sortObjectArrayByNumber;

    function generateBoolQuery(filters) {
      var query =
        {
          bool: {
            filter: []
          }
        };
      if (filters) {
        angular.forEach(filters, function (filter) {
          query.bool.filter.push(
            {
              bool: filter
            }
          );
        });
      }
      return query;
    }

    function shorten(text) {
      var limit = 47;
      if(text) {
        if (text.length > limit) {
          return text.substring(0, Math.max(1, limit - 3)) + '...';
        } else {
          return text;
        }
      } else {
        return '';
      }
    }

    function sortObjectArray(array, field) {
      if (array) {
        array.sort(function (a, b) {
          return a[field].localeCompare(b[field]);
        });
        return array;
      }
    }

    function sortObjectArrayByNumber(array, field) {
      if (array) {
        array.sort(function (a, b) {
          return a[field] - b[field];
        });
        return array;
      }
    }
  }

  angular
    .module('emgb2.Utils')
    .directive("fileUpload", [function () {
      return {
        scope: {
          contents: "="
        },
        link: function (scope, element, attributes) {
          element.bind("change", function (changeEvent) {
            var reader = new FileReader();
            reader.onload = function (loadEvent) {
              scope.$apply(function () {
                scope.contents = loadEvent.target.result;
                changeEvent.target.value = "";
              });
            }
            reader.readAsText(changeEvent.target.files[0]);
          });
        }
      }
    }]);
})();
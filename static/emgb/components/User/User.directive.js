/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.User')
    .directive("user", user);

  function user() {
    return {
      restrict: 'A',
      template: '<span></span>',
      controller: function ($scope, $rootScope, $http, AUTH_ECHO_URL) {
        if (AUTH_ECHO_URL === '') {
          $scope.username = "anonymous";
          $scope.authHeader = "anonymous";
        } else {
          $http(
            {
              method: 'GET',
              url: AUTH_ECHO_URL
            })
            .then(function (resp) {
              $scope.username = resp.data.username;
              $scope.authHeader = resp.data.header;
            }, function (err) {
              console.log(err);
            })
            .finally(function () {
            });
        }

        $rootScope.getUsername = function () {
          return $scope.username;
        };
        $rootScope.getAuthHeader = function () {
          return $scope.authHeader;
        };
      }
    };
  }
})();
/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.TextQuery')
    .controller('TextQuery', TextQuery);

  function TextQuery($scope) {
    var textQuery = this;

    textQuery.queryString = "";
    textQuery.focus = false;

    textQuery.submitQuery = submitQuery;
    textQuery.broadcastQueryChange = broadcastQueryChange;
    textQuery.clear = clear;

    $scope.$on('TextQuery:changed', function (event, newQueryString) {
      textQuery.queryString = newQueryString;
      textQuery.submitQuery();
      textQuery.focus = false;
    });

    function submitQuery() {
      $scope.$emit('ResultFilters:changed:relay', 'TextQuery', {
        must: {
          query_string: {
            query: textQuery.queryString,
            default_operator: "and"
          }
        }
      });
    }

    function broadcastQueryChange() {
      if (textQuery.queryString === "") {
        textQuery.queryString = "*:*";
      }
      $scope.$emit('TextQuery:changed:relay', textQuery.queryString);
    }

    function clear() {
      textQuery.queryString = "";
      textQuery.broadcastQueryChange();
      textQuery.queryString = "";
      textQuery.focus = true;
    }
  }

})();

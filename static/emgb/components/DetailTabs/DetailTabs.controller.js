/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.DetailTabs')
    .controller('DetailTabs', DetailTabs);

  function DetailTabs($scope, $timeout, ENABLED_INSIGHTS) {
    var detailTabs = this;

    detailTabs.loadingCounts = {};
    detailTabs.activeTabIndex = 0;
    detailTabs.activeTabLabel = 'table';
    detailTabs.enabledInsights = ENABLED_INSIGHTS;

    detailTabs.tabSelected = tabSelected;
    detailTabs.isLoading = isLoading;

    $scope.$on('DetailTabs:loading', function (event, tabLabel, loading) {
      event.stopPropagation && event.stopPropagation();
      initLoading(tabLabel);
      detailTabs.loadingCounts[tabLabel] += loading ? 1 : -1;
    });

    $scope.$on('DetailTabs:forceSelection', function (event, tabLabel, metadata) {
      event.stopPropagation && event.stopPropagation();
      tabSelected(tabLabel);
      if (tabLabel === 'pathway-overview') {
        detailTabs.activeTabIndex = 3;
        tabSelected(tabLabel);
        $timeout(function () {
          $scope.$broadcast('PathwayOverview:selectPathway', metadata);
        });
      }
    });

    function tabSelected(tabLabel) {
      detailTabs.activeTabLabel = tabLabel;
      $timeout(function () {
        $scope.$broadcast('DetailTabs:changed', tabLabel);
      });
    }

    function initLoading(tabLabel) {
      if (typeof detailTabs.loadingCounts[tabLabel] === 'undefined') {
        detailTabs.loadingCounts[tabLabel] = 0;
      }
    }

    function isLoading(tabLabel) {
      initLoading(tabLabel);
      return detailTabs.loadingCounts[tabLabel] > 0;
    }
  }
})();

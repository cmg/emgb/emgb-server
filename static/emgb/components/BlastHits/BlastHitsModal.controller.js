/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.BlastHits')
    .controller('BlastHitsModalCtrl', BlastHitsModalCtrl);

  function BlastHitsModalCtrl($scope, $rootScope, $uibModal, $uibModalInstance, $timeout, geneid, dataset, ElasticSearch, BLAST_TABLE_COLUMN_LABELS) {
    var bhModalCtrl = this;

    bhModalCtrl.geneid = geneid;
    bhModalCtrl.dataset = dataset;
    bhModalCtrl.columns = ["salltitles", "evalue", "pident", "bitscore", "length", "sacc"];
    bhModalCtrl.hits = [];
    bhModalCtrl.loading = false;

    bhModalCtrl.getColumnLabel = getColumnLabel;
    bhModalCtrl.close = close;

    function loadBlastHits() {
      $rootScope.startSpinner();
      bhModalCtrl.loading = true;
      ElasticSearch.getGene(bhModalCtrl.dataset.name, geneid, ['geneid', 'blast'])
        .then(function (resp) {
          bhModalCtrl.hits = resp.hits.hits[0]._source['blast'];
          if (bhModalCtrl.hits) {
            bhModalCtrl.hits.sort(function (a, b) {
              return b.bitscore - a.bitscore;
            });
          } else {
            bhModalCtrl.hits = [];
          }
        }, function (err) {
          console.trace(err.message);
        })
        .finally(function () {
          bhModalCtrl.loading = false;
          $rootScope.stopSpinner();
        });
    }

    function getColumnLabel(column) {
      return BLAST_TABLE_COLUMN_LABELS[column] || column.charAt(0).toUpperCase() + column.slice(1);
    }

    function close() {
      $uibModalInstance.dismiss('close');
    }

    loadBlastHits();
  }
})();

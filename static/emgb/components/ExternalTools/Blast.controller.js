/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.ExternalTools')
    .controller('BlastCtrl', BlastCtrl);

  function BlastCtrl($scope, $uibModalInstance, $http, $timeout, HOBIT_STATUS_CODES, blastType, geneId, sequence, dataset) {
    var blastCtrl = this;

    $scope.blastType = blastType;
    $scope.geneId = geneId;
    $scope.sequence = sequence;
    $scope.statuscode;
    $scope.downloadUrl;
    $scope.result = '';

    $scope.isLoading = isLoading;
    $scope.isSuccess = isSuccess;
    $scope.getHobitStatus = getHobitStatus;

    var timeout = 250;
    var downloadBlob;

    var emgbBlastUrl = 'http://gonzo:9080/rest/emgbblast/emgbblast_blast';

    blastCtrl.jobParameters = {
      paramset: {
        emgbblast_param_db: dataset.name,
        emgbblast_param_viewoptions: '8'
      }
    };
    if (blastType === 'p') {
      blastCtrl.jobParameters.emgbblast_input_aa = '>' + geneId + "\n" + sequence;
    } else {
      blastCtrl.jobParameters.emgbblast_input_na = '>' + geneId + "\n" + sequence;
    }

    $http(
      {
        method: 'POST',
        url: emgbBlastUrl + $scope.blastType + '/request',
        headers: {
          'Accept': 'text/plain'
        },
        data: blastCtrl.jobParameters
      })
      .then(function (res) {
        waitForResult(res.data);
      }, function (err) {
        $scope.statuscode = 0;
      });


    function waitForResult(jobId) {
      $http(
        {
          method: 'POST',
          url: emgbBlastUrl + $scope.blastType + '/statuscode',
          headers: {
            'Content-Type': 'text/plain'
          },
          data: jobId
        })
        .then(function (res) {
          $scope.statuscode = parseInt(res.data);
          if (isSuccess()) {
            loadResult(jobId);
          } else if (isLoading()) {
            $timeout(function () {
              waitForResult(jobId);
              if (timeout < 10000) {
                timeout = timeout * 2;
              }
            }, timeout);
          }
        }, function (err) {
          $scope.statuscode = err.status;
        })
        .finally(function () {
        });
    }

    function loadResult(jobId) {
      $http(
        {
          method: 'POST',
          url: emgbBlastUrl + $scope.blastType + '/response',
          headers: {
            'Content-Type': 'text/plain',
            'Accept': 'text/plain'
          },
          data: jobId
        })
        .then(function (res) {
          $scope.statuscode = res.status;
          $scope.result = res.data;
          downloadBlob = new Blob([res.data], {type: 'text/plain'});
          $scope.downloadUrl = (window.URL || window.webkitURL).createObjectURL(downloadBlob);
          $uibModalInstance.closed.then(function () {
            (window.URL || window.webkitURL).revokeObjectURL(downloadBlob);
          });
        }, function (err) {
          $scope.statuscode = err.status;
        });
    }

    function isLoading() {
      return $scope.statuscode > 600 && $scope.statuscode <= 699;
    }

    function isSuccess() {
      return $scope.statuscode === 600 || $scope.statuscode === 200;
    }

    function getHobitStatus(code) {
      return HOBIT_STATUS_CODES[code] || '' + code;
    }

    $scope.close = function () {
      $uibModalInstance.dismiss('close');
    };
  }
})();

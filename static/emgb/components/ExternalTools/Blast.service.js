/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.ExternalTools')
    .service('Blast', Blast);

  function Blast($uibModal, $timeout) {
    var blast = this;

    blast.status = 'Initializing...';

    blast.run = run;

    function run(blastType, geneId, sequence, dataset) {
      $uibModal.open({
        animation: false,
        backdrop: 'static',
        templateUrl: 'components/ExternalTools/blast.html',
        controller: 'BlastCtrl',
        resolve: {
          blastType: function () {
            return blastType;
          },
          geneId: function () {
            return geneId;
          },
          sequence: function () {
            return sequence;
          },
          dataset: function () {
            return dataset;
          }
        }
      });
    }
  }
})();

/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.ExternalTools')
    .service('ExternalTools', ExternalTools);

  function ExternalTools(Blast) {
    var externalTools = this;

    externalTools.runBlast = runBlast;

    function runBlast(blastType, geneId, sequence, dataset) {
      Blast.run(blastType, geneId, sequence.replace('*', ''), dataset);
    }
  }

})();

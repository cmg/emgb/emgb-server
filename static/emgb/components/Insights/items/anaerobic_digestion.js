/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.Insights')
    .controller('anaerobic_digestion', AnaerobicDigestion);
  angular
    .module('emgb2.Insights')
    .directive('adEc', AnaerobicDigestionEC);

  function AnaerobicDigestion($uibModalInstance, dataset, insightId, insightLabel, ElasticSearch, $rootScope) {
    var ad = this;

    ad.insightId = insightId;
    ad.insightLabel = insightLabel;
    ad.dataset = dataset;
    ad.bins = ['no filter'];
    ad.filterBin = 'no filter';
    ad.binsLoading = true;

    ad.close = close;

    function close() {
      $uibModalInstance.dismiss('close');
    }

    function loadBins() {
      $rootScope.startSpinner();
      ElasticSearch.getBinsAggregration(dataset.name, {match_all: {}})
        .then(function (resp) {
          var binIDs = [];
          angular.forEach(resp.aggregations.binsAggregration.bins.buckets, function (bucket) {
            binIDs.push(bucket.key);
          });
          binIDs = _.sortBy(binIDs, [function (o) {
            var n = parseInt(o, 10);
            if (isNaN(n)) {
              return o;
            } else {
              return n;
            }
          }]);
          ad.bins = _.concat('no filter', binIDs);
        }, function (err) {
          console.trace(err.message);
        })
        .finally(function () {
          ad.binsLoading = false;
          $rootScope.stopSpinner();
        });
    }

    loadBins();
  }

  function AnaerobicDigestionEC(FilterBox) {
    return {
      restrict: 'E',
      scope: {
        dataset: '=',
        ad: '=',
        ec: '='
      },
      template: '<span style="font-family: monospace" uib-tooltip="Add filter" tooltip-placement="right">EC {{ecDisplay}}</span> <span class="badge" style="cursor:pointer" ng-click="addECFilterBox()"><i ng-show="count === undefined" class="fa fa-fw fa-spinner fa-pulse"></i>{{count}}</span>',
      controller: function ($scope, ElasticSearch) {
        function render() {
          $scope.count = undefined;
          $scope.ecDisplay = $scope.ec.endsWith('.') ? $scope.ec.substr(0, $scope.ec.length - 1) : $scope.ec;
          var filterBin = $scope.ad.filterBin === 'no filter' ? undefined : $scope.ad.filterBin;
          ElasticSearch.getEcCount($scope.dataset.name, $scope.ec, filterBin)
            .then(function (resp) {
              $scope.count = resp.hits.total;
            }, function (err) {
              console.trace(err.message);
            })
            .finally(function () {
            });
        }

        $scope.addECFilterBox = function () {
          var compOp = 'exactly';
          var filterEc = $scope.ec;
          filterEc = 'ec:' + filterEc;
          if (filterEc[filterEc.length - 1] === '.') {
            filterEc = filterEc.replace(/\./g, '\\.');
            filterEc = filterEc + '.*';
            compOp = 'regex';
          }
          var filterBox = new FilterBox('AD filter for ECs: ' + $scope.ec);
          filterBox.addFilter('ecs', filterEc, compOp);
          $scope.$emit('FilterBoxes:add:relay', filterBox);
          if ($scope.ad.filterBin !== 'no filter') {
            filterBox.operator = 'AND';
            filterBox.addFilter('binning.bins', $scope.ad.filterBin);
          }
        };

        render();

        $scope.$watch(angular.bind($scope.ad.filterBin, function () {
          return $scope.ad.filterBin;
        }), function () {
          render();
        });
      }
    };
  }
})();

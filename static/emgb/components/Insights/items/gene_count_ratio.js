/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.Insights')
    .controller('gene_count_ratio', GeneCountRatio);

  function GeneCountRatio(ElasticSearch, $rootScope, $timeout, PHYLO_TREE_RANK_LABELS, $uibModalInstance, dataset, insightId, insightLabel) {
    var geneCountRatio = this;

    geneCountRatio.insightId = insightId;
    geneCountRatio.insightLabel = insightLabel;
    geneCountRatio.close = close;
    geneCountRatio.availableRanks = Object.keys(PHYLO_TREE_RANK_LABELS);
    geneCountRatio.selectedRank = '';
    geneCountRatio.loading = false;
    geneCountRatio.threshold = 0.01;
    geneCountRatio.config = {
      deepWatchData: false
    };
    geneCountRatio.chart = {
      data: [],
      api: {},
      options: {
        chart: {
          type: "multiBarHorizontalChart",
          margin: {
            top: 20,
            right: 30,
            bottom: 60,
            left: 240
          },
          tooltip: {
            valueFormatter: function (d) {
              return d.toFixed(2) + " %";
            },
            headerEnabled: false
          },
          valueFormat: function (n) {
            return n.toFixed(2) + " %";
          },
          height: 1200,
          clipEdge: true,
          duration: 300,
          stacked: false,
          showValues: true,
          showControls: false,
          xAxis: {
            axisLabel: ""
          },
          yAxis: {
            axisLabel: "%",
            tickFormat: function (d) {
              return d;
            }
          },
          noData: ""
        },
        title: {
          enable: true,
          text: "Gene Count Ratio"
        }
      }
    };
    geneCountRatio.chart.options.chart.xAxis.tickFormat = function (d) {
      if (tickLabels[d]) {
        return tickLabels[d];
      }
      return d;
    };

    geneCountRatio.getRankLabel = getRankLabel;
    geneCountRatio.selectRank = selectRank;

    var tickLabels = [];
    var countLabels = [];

    function fetchCountLabels() {
      countLabels = [];
      $rootScope.startSpinner();
      geneCountRatio.loading = true;
      ElasticSearch.getGeneCountLabels(dataset.name)
        .then(function (resp) {
          angular.forEach(resp.aggregations.countLabels.buckets, function (bucket) {
            countLabels.push(bucket.key);
          });
        }, function (err) {
          console.trace(err.message);
        })
        .finally(function () {
          geneCountRatio.loading = false;
          $rootScope.stopSpinner();
        });
    }

    function updateChart() {
      $rootScope.startSpinner();
      geneCountRatio.loading = true;
      ElasticSearch.getGeneCountSumsForPhyloLevel(dataset.name, geneCountRatio.selectedRank, countLabels)
        .then(function (resp) {
          var maxOfCurrentSums = {};
          angular.forEach(countLabels, function (currentCountLabel, j) {
            angular.forEach(resp.aggregations['phyloLevel_' + currentCountLabel].buckets, function (bucket, i) {
              if (maxOfCurrentSums[currentCountLabel] === undefined) {
                maxOfCurrentSums[currentCountLabel] = {};
              }
              if (maxOfCurrentSums[currentCountLabel][bucket.key] === undefined) {
                maxOfCurrentSums[currentCountLabel][bucket.key] = 1;
              }
              maxOfCurrentSums[currentCountLabel][bucket.key] = Math.max(maxOfCurrentSums[currentCountLabel][bucket.key], bucket.insideCount.byCountLabel.geneCountSum.value);
            });
          });
          var tickGetsRendered = {};
          angular.forEach(countLabels, function (currentCountLabel, j) {
            var overallSum = resp.aggregations['insideCount_' + currentCountLabel].byCountLabel.overallCountSum.value;
            angular.forEach(resp.aggregations['phyloLevel_' + currentCountLabel].buckets, function (bucket, i) {
              if (tickGetsRendered[bucket.key] === undefined) {
                tickGetsRendered[bucket.key] = false;
              }
              if (tickGetsRendered[bucket.key] === false) {
                tickGetsRendered[bucket.key] = maxOfCurrentSums[currentCountLabel][bucket.key] / overallSum > geneCountRatio.threshold;
              }
            });
          });
          var bucketCount = 0;
          angular.forEach(countLabels, function (currentCountLabel, j) {
            tickLabels = [];
            geneCountRatio.chart.data[j] = {key: "", values: []};
            var overallSum = resp.aggregations['insideCount_' + currentCountLabel].byCountLabel.overallCountSum.value;
            angular.forEach(resp.aggregations['phyloLevel_' + currentCountLabel].buckets, function (bucket, i) {
              tickLabels.push(bucket.key);
              if (tickGetsRendered[bucket.key]) {
                var currentSum = bucket.insideCount.byCountLabel.geneCountSum.value;
                geneCountRatio.chart.data[j].key = currentCountLabel;
                geneCountRatio.chart.data[j].values.push({x: i, y: (currentSum / overallSum) * 100});
                bucketCount++;
              }
            });
          });
          geneCountRatio.chart.options.title.text = "Gene Count Ratio (" + geneCountRatio.getRankLabel(geneCountRatio.selectedRank) + " level, >1%)";
          geneCountRatio.chart.options.chart.height = 150 + (16 * bucketCount);
          geneCountRatio.chart.api.update();
        }, function (err) {
          console.trace(err.message);
        })
        .finally(function () {
          geneCountRatio.loading = false;
          $rootScope.stopSpinner();
        });
    }

    function getRankLabel(rank) {
      return PHYLO_TREE_RANK_LABELS[rank];
    }

    function selectRank(rank) {
      if (!geneCountRatio.loading) {
        geneCountRatio.selectedRank = rank;
        updateChart();
      }
    }

    function close() {
      $uibModalInstance.dismiss('close');
    }

    $timeout(function () {
      if (countLabels.length === 0) {
        fetchCountLabels();
      }
      geneCountRatio.chart.api.update();
    });

  }

})();

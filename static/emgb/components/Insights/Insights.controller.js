/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.Insights')
    .controller('Insights', Insights);

  function Insights($scope, $uibModal, ENABLED_INSIGHTS, INSIGHT_LABELS) {
    var insights = this;

    insights.insightOptions = ENABLED_INSIGHTS;
    insights.getLabel = getLabel;
    insights.openInsight = openInsight;

    function getLabel(insightId) {
      return INSIGHT_LABELS[insightId];
    }

    function openInsight(insightId) {
      $uibModal.open({
        animation: false,
        backdrop: true, //'static',
        templateUrl: 'components/Insights/items/' + insightId + '.html',
        controller: insightId,
        controllerAs: insightId,
        size: 'lg',
        scope: $scope,
        resolve: {
          insightId: function () {
            return insightId;
          },
          insightLabel: function () {
            return getLabel(insightId);
          },
          dataset: function () {
            return $scope.dataset;
          }
        }
      });
    }

  }
})();

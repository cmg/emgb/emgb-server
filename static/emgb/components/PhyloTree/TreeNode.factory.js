/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.PhyloTree')
    .factory('TreeNode', TreeNode);

  function TreeNode(ElasticSearch, $rootScope, PHYLO_TREE_HIERARCHY, PHYLO_TREE_RANK_LABELS) {
    var TreeNode = function (index, parent, field, label, count, sortByLabel) {
      this.index = index;
      this.parent = parent;
      this.field = field;
      this.label = label;
      this.name = label; // needed for nvd3 sunburst
      this.count = count;
      this.size = count; // needed for nvd3 sunburst
      this.children = [];
      this.expanded = false;
      this.loaded = false;
      this.selected = false;
      this.partiallySelected = false;
      this.sortByLabel = sortByLabel;
      this.expansionIsLoading = false;
      this.countIsUpdating = false;
    };

    TreeNode.prototype.createChildren = function (buckets) {
      var parent = this;
      angular.forEach(buckets, function (bucket) {
        var child = new TreeNode(parent.index, parent, PHYLO_TREE_HIERARCHY[parent.field], bucket.key, bucket.doc_count, parent.sortByLabel);
        child.selected = parent.selected;
        parent.children.push(child);
      });
    };

    TreeNode.prototype.getRankLabel = function () {
      return PHYLO_TREE_RANK_LABELS[this.field];
    };

    TreeNode.prototype.toggle = function (callback, textQuery) {
      if (this.loaded !== true) {
        this.loadExpand(callback, textQuery);
      } else {
        this.expanded = !this.expanded;
      }
    };

    TreeNode.prototype.toggleSort = function (callback, textQuery) {
      var node = this;
      node.sortByLabel = !node.sortByLabel;
      node.children.sort(node.compareChildren);
      angular.forEach(this.children, function (child) {
        if (!child.loaded) {
          child.sortByLabel = node.sortByLabel;
        }
      });
    };

    function createHierarchyFilter(node) {
      var hierarchyFilter = [];
      var currentNode = node;
      while (currentNode.parent) {
        var filterElement = {term: {}};
        filterElement.term[currentNode.field] = currentNode.label;
        hierarchyFilter.push(filterElement);
        currentNode = currentNode.parent;
      }
      return hierarchyFilter;
    }

    TreeNode.prototype.loadExpand = function (callback, textQuery) {
      var childField = PHYLO_TREE_HIERARCHY[this.field];
      if (childField) {
        var node = this;
        node.expansionIsLoading = true;
        $rootScope.startSpinner();
        ElasticSearch.getPhyloAggregation(this.index, childField, createHierarchyFilter(this), null)
          .then(function (resp) {
            node.createChildren(resp.aggregations.phylo.buckets);
            node.expanded = true;
            node.loaded = true;
            node.markCountsAsUpdating(false);
            node.updateCounts(textQuery, callback);
          }, function (err) {
            console.trace(err.message);
          })
          .finally(function () {
            node.expansionIsLoading = false;
            $rootScope.stopSpinner();
          });
      }
    };

    TreeNode.prototype.markCountsAsUpdating = function (markSelf) {
      if (markSelf) {
        this.countIsUpdating = true;
      }
      angular.forEach(this.children, function (child) {
        child.markCountsAsUpdating(true);
      });
    };

    TreeNode.prototype.updateCounts = function (textQuery, callback) {
      var childField = PHYLO_TREE_HIERARCHY[this.field];
      if (childField) {
        var node = this;
        $rootScope.startSpinner();
        ElasticSearch.getPhyloAggregation(this.index, childField, createHierarchyFilter(this), textQuery)
          .then(function (resp) {
            angular.forEach(node.children, function (child) {
              var labelFound = false;
              angular.forEach(resp.aggregations.phylo.buckets, function (bucket) {
                if (bucket.key === child.label) {
                  child.count = bucket.doc_count;
                  labelFound = true;
                }
              });
              if (!labelFound) {
                child.count = 0;
              }
              child.size = child.count; // needed for nvd3 sunburst
              child.countIsUpdating = false;
              if (child.loaded) {
                child.updateCounts(textQuery, callback);
              }
            });
            node.children.sort(node.compareChildren);
          }, function (err) {
            console.trace(err.message);
          })
          .finally(function () {
            $rootScope.stopSpinner();
            if (!$rootScope.isLoading() && callback) {
              callback();
            }
          });
      }
    };

    TreeNode.prototype.isLeaf = function () {
      if (!PHYLO_TREE_HIERARCHY[this.field]) {
        return true;
      } else {
        if (this.loaded) {
          return this.children.length === 0;
        } else {
          return false;
        }
      }
    };

    TreeNode.prototype.toggleSelect = function () {
      this.select(!this.selected);
    };

    TreeNode.prototype.select = function (selected) {
      this.selected = selected;
      this.propagateSelectionDown(selected);
      this.parent.propagateSelectionUp();
    };

    TreeNode.prototype.propagateSelectionDown = function (selected) {
      angular.forEach(this.children, function (child) {
        child.select(selected);
      });
    };

    TreeNode.prototype.propagateSelectionUp = function () {
      var hasSelectedChildren = false;
      var allChildrenSelected = true;
      angular.forEach(this.children, function (child) {
        if (child.selected || child.partiallySelected) {
          hasSelectedChildren = true;
        }
        if (!child.selected) {
          allChildrenSelected = false;
        }
      });
      if (allChildrenSelected) {
        this.partiallySelected = false;
        this.selected = true;
      } else {
        this.partiallySelected = hasSelectedChildren;
        this.selected = false;
      }
      if (this.parent.propagateSelectionUp) {
        this.parent.propagateSelectionUp();
      }
    };

    TreeNode.prototype.getSelectionHierarchyFilters = function () {
      var fields = [];
      if ((this.children.length === 0 || !this.parent.selected) && this.selected) {
        return [{bool: {filter: createHierarchyFilter(this)}}];
      } else {
        angular.forEach(this.children, function (child) {
          angular.forEach(child.getSelectionHierarchyFilters(), function (filter) {
            fields.push(filter);
          });
        });
      }
      return fields;
    };

    // Mainly used for percentage calculation because root count variable is not set
    TreeNode.prototype.getChildrenCountSum = function () {
      var childrenCountSum = 0;
      angular.forEach(this.children, function (child) {
        childrenCountSum += child.count;
      });
      return childrenCountSum;
    };

    // Comparison function for the node children
    // which is used by the list sorting algorithm.
    TreeNode.prototype.compareChildren = function (a, b) {
      if (a.parent.sortByLabel) {
        return a.label.localeCompare(b.label);  // sort by node label (asc)
      } else {
        return b.count - a.count;  // sort by gene count (desc)
      }
    };

    return TreeNode;
  }
})();

/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.PhyloTree')
    .controller('PhyloTree', PhyloTree);

  function PhyloTree(TreeNode, $scope, $rootScope, $timeout) {
    var tree = this;

    tree.textQuery;
    tree.sunburstVisible = false;
    tree.showPercentages = false;
    tree.sunburstApi;
    tree.sunburstOptions = {
      chart: {
        type: 'sunburstChart',
        height: 320,
        duration: 350,
        sunburst: {
          mode: 'size'
        },
        tooltip: {
          enabled: false
        },
        noData: ''
      }
    };
    tree.sunburstConfig = {
      refreshDataOnly: true,
      deepWatchData: false
    };
    tree.sunburstData = [];
    tree.debounceMillis = 1500;
    tree.treeNodeClickedPromise;
    tree.clickCascadeCount = 0;

    tree.treeNodeClicked = treeNodeClicked;
    tree.updateSunburst = updateSunburst;
    tree.resetSelection = resetSelection;

    init();

    $scope.$on('DatasetTab:updateLayout', function () {
      updateSunburst();
    });

    function init() {
      tree.root = new TreeNode($scope.dataset.name, false, 'root', '', null, false);
      tree.root.loadExpand(updateSunburst, tree.textQuery);
    }

    $scope.$on('TextQuery:changed', function (event, textQueryString) {
      tree.textQuery = {query_string: {query: textQueryString, default_operator: "and"}};
      tree.root.markCountsAsUpdating(false);
      tree.root.updateCounts(tree.textQuery, tree.updateSunburst);
    });

    function treeNodeClicked() { // filter changes on tree selection
      if (tree.clickCascadeCount === 0) {
        notifyUpdate();
        tree.clickCascadeCount += 1;
        tree.treeNodeClickedPromise = $timeout(function () {
          tree.clickCascadeCount = 0;
        }, tree.debounceMillis);
      } else {
        if (tree.treeNodeClickedPromise) {
          if ($timeout.cancel(tree.treeNodeClickedPromise)) {
            if (tree.clickCascadeCount > 1) {
              $rootScope.stopSpinner();
            }
            tree.clickCascadeCount += 1;
          }
        }
        $rootScope.startSpinner();
        tree.treeNodeClickedPromise = $timeout(function () {
          notifyUpdate();
          $rootScope.stopSpinner();
          tree.clickCascadeCount = 0;
        }, tree.debounceMillis);
      }
    }

    function resetSelection() {
      tree.root.propagateSelectionDown(false);
      notifyUpdate();
    }

    function notifyUpdate() {
      $scope.$emit('ResultFilters:changed:relay', 'PhyloTree', {should: tree.root.getSelectionHierarchyFilters()}); // notifies parent tab
    }

    function updateSunburst() {
      tree.sunburstData = [tree.root];
      tree.sunburstApi.refresh();
    }
  }
})();

/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.FilterBoxes')
    .directive('filterBoxEditor', FilterBoxEditor);

  function FilterBoxEditor(FilterBox, ElasticSearch, $rootScope, PAGINATED_TABLE_COLUMN_LABELS, FIELD_COMPARISON_OPERATORS) {
    return {
      scope: {dataset: '=dataset'},
      restrict: "E",
      replace: true,
      templateUrl: 'components/FilterBoxes/filter-box-editor.html',
      link: function (scope, element, attrs) {
        scope.editing = false;
        scope.availableFields = [];
        scope.getFieldType = function (field) {
          var fieldType = 'keyword';
          for (var j = 0; j < scope.availableFields.length; j++) {
            if (scope.availableFields[j].name === field) {
              fieldType = scope.availableFields[j].type;
              break;
            }
          }
          return fieldType;
        };
        scope.getAvailableComparisonOperators = function (field) {
          return FIELD_COMPARISON_OPERATORS[scope.getFieldType(field)];
        };
        scope.editBox = function (box) {
          if (!box) {
            box = new FilterBox();
            box.addFilter('', '');
          }
          scope.editedBox = box;
          scope.editedBox.label = '';
          scope.editing = true;
        };
        scope.saveBox = function () {
          for (var i = 0; i < scope.editedBox.filters.length; i++) {

            switch (scope.getFieldType(scope.editedBox.filters[i].field)) {
              case 'integer':
              case 'double':
                scope.editedBox.filters[i].value.single = Number(scope.editedBox.filters[i].value.single);
                break;
              case 'keyword':
              case 'text':
              case 'string':
                break;
            }
          }
          scope.$emit('FilterBoxes:add:relay', scope.editedBox);
          scope.editing = false;
        };
        scope.abortEdit = function () {
          scope.editedBox = new FilterBox();
          scope.editedBox.addFilter('', '');
          scope.editing = false;
        };
        scope.toggleOperator = function () {
          if (scope.editedBox.operator === 'OR') {
            scope.editedBox.operator = 'AND';
          } else {
            scope.editedBox.operator = 'OR';
          }
        };
        scope.$on('FilterBoxEditor:edit', function (event, box) {
          if (scope.editing) {
            scope.saveBox();
          }
          scope.editBox(box);
        });
        scope.loadAvailableFields = function () {
          $rootScope.startSpinner();
          ElasticSearch.getMapping(scope.dataset.name)
            .then(function (resp) {
              scope.availableFields = nestedPropertiesToFlatList(resp[scope.dataset.name].mappings.gene.properties);
              scope.sortAvailableFields();
            }, function (err) {
              console.trace(err.message);
            })
            .finally(function () {
              $rootScope.stopSpinner();
            });
        };
        scope.sortAvailableFields = function () {
          scope.availableFields.sort(function (a, b) {
            return scope.getFieldLabel(a.name).localeCompare(scope.getFieldLabel(b.name));
          });
        };
        scope.getFieldLabel = function (field) {
          return PAGINATED_TABLE_COLUMN_LABELS[field] || field.charAt(0).toUpperCase() + field.slice(1);
        };
        scope.formatComparisonOperator = function (comparisonOperator) {
          return 'is ' + comparisonOperator;
        };
        scope.loadAvailableFields();
        var nestedPropertiesToFlatList = function (properties) {
          var flatList = [];
          angular.forEach(properties, function (value, key) {
            flatList = flatList.concat(flattenProperty(key, value));
          });
          return flatList;
        };
        var flattenProperty = function (prefix, property) {
          if (property.type === 'nested') {
            var children = [];
            angular.forEach(property.properties, function (value, key) {
              children = children.concat(flattenProperty(prefix + '.' + key, value));
            });
            return children;
          } else {
            return [{name: prefix, type: property.type}];
          }
        };
      }
    };
  }

})();

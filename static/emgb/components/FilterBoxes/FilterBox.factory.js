/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.FilterBoxes')
    .factory('FilterBox', FilterBox);

  function FilterBox() {
    var FilterBox = function (label, operator) {
      this.label = label || '';
      this.filters = [];
      this.operator = operator || 'OR';
      this.opacity = 0.1;
      this.enabled = true;
    };

    FilterBox.prototype.addFilter = function (field, value, comparisonOperator) {
      this.filters.push(
        {
          field: field,
          value: {
            single: value,
            range: {
              from: value,
              to: value
            }
          },
          comparisonOperator: comparisonOperator || 'exactly'
        }
      );
    };

    FilterBox.prototype.removeFilter = function (index) {
      if (index >= 0 && index < this.filters.length) {
        this.filters.splice(index, 1);
      }
    };

    FilterBox.prototype.buildQueryString = function () {
      var queryString = '';
      if (this.filters.length > 0) {
        queryString += '(';
      }
      for (var i = 0; i < this.filters.length; i++) {
        queryString += buildQueryField(this.filters[i]);
        if (i < this.filters.length - 1) {
          queryString += ' ' + this.operator + ' ';
        }
      }
      if (this.filters.length > 0) {
        queryString += ')';
      }
      return queryString;
    };

    function buildQueryField(filter) {
      var queryField = '';
      if (filter.comparisonOperator === 'exactly not') {
        queryField += 'NOT ';
      }
      if (filter.comparisonOperator === 'present' || filter.comparisonOperator === 'absent') {
        if (filter.comparisonOperator === 'absent') {
          queryField += 'NOT ';
        }
        queryField += '_exists_';
      } else {
        queryField += filter.field;
      }
      queryField += ':';
      if (filter.comparisonOperator === 'present' || filter.comparisonOperator === 'absent') {
        queryField += filter.field;
      } else {
        if (filter.comparisonOperator === 'exactly' || filter.comparisonOperator === 'exactly not') {
          queryField += '"';
        } else if (filter.comparisonOperator === 'regex') {
          queryField += '/';
        } else if (filter.comparisonOperator === 'greater than') {
          queryField += '>';
        } else if (filter.comparisonOperator === 'less than') {
          queryField += '<';
        }
        if (filter.comparisonOperator === 'in range') {
          queryField += '[';
          queryField += filter.value.from;
          queryField += ' TO ';
          queryField += filter.value.to;
          queryField += ']';
        } else {
          queryField += filter.value.single;
        }
        if (filter.comparisonOperator === 'exactly' || filter.comparisonOperator === 'exactly not') {
          queryField += '"';
        } else if (filter.comparisonOperator === 'regex') {
          queryField += '/';
        }
      }
      return queryField;
    }

    FilterBox.prototype.toggle = function () {
      this.enabled = !this.enabled;
    };

    return FilterBox;
  }
})();

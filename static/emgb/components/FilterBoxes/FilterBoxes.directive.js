/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.FilterBoxes')
    .directive('filterBoxes', FilterBoxes);

  function FilterBoxes($timeout, PAGINATED_TABLE_COLUMN_LABELS, Utils) {
    return {
      scope: {dataset: '=dataset'},
      restrict: "E",
      replace: true,
      templateUrl: 'components/FilterBoxes/filter-boxes.html',
      link: function (scope, element, attrs) {
        var nextBoxId = 0;
        scope.boxes = [];
        scope.cloneButtonVisible = false;
        var numberOfOpenDatasetTabs = 1;
        scope.filterCloneModeOn = false;

        function evaluateCloneButtonVisible() {
          if (scope.boxes.length === 0 && numberOfOpenDatasetTabs > 1) {
            scope.cloneButtonVisible = true;
          } else {
            scope.cloneButtonVisible = false;
          }
        }

        scope.$on('DatasetTabs:tabCount', function (event, tabCount) {
          numberOfOpenDatasetTabs = tabCount;
          evaluateCloneButtonVisible();
        });
        scope.$emit('DatasetTabs:requestTabCount');

        scope.toggleFilterCloneMode = function () {
          scope.filterCloneModeOn = !scope.filterCloneModeOn;
          scope.$emit('DatasetTabs:filterCloneMode', scope.filterCloneModeOn, scope.dataset.id);
        };

        scope.$on('DatasetTab:requestFilters', function (event, datasetId) {
          if (datasetId === scope.dataset.id) {
            var boxesCopy = angular.copy(scope.boxes);
            scope.$emit('DatasetTab:filters:relay', boxesCopy);
          }
        });

        scope.$on('DatasetTab:filters', function (event, filters) {
          if (scope.filterCloneModeOn) {
            $timeout(function () {
              scope.boxes = filters;
              angular.forEach(scope.boxes, function (box) {
                if (nextBoxId <= box.id) {
                  nextBoxId = box.id + 1;
                }
              });
              scope.filterCloneModeOn = false;
              scope.$emit('DatasetTabs:filterCloneMode', scope.filterCloneModeOn, -1);
              $timeout(function () {
                generateQueryString();
                evaluateCloneButtonVisible();
              }, 50);
            });
          }
        });

        scope.$on('FilterBoxes:add', function (event, newBox) {
          var alreadyExists = false;
          for (var i = 0; i < scope.boxes.length; i++) {
            if (angular.equals(scope.boxes[i].filters, newBox.filters)) {
              alreadyExists = true;
              break;
            }
          }
          if (!alreadyExists) {
            newBox.id = nextBoxId++;
            scope.boxes.push(newBox);
            $timeout(function () {
              newBox.opacity = 1;
              scope.filterCloneModeOn = false;
              generateQueryString();
              evaluateCloneButtonVisible();
              scope.$emit('DatasetTabs:filterCloneMode', scope.filterCloneModeOn, -1);
            }, 50);
          }
        });
        scope.removeBox = function (boxToRemove, callback) {
          boxToRemove.opacity = 0;
          $timeout(function () {
            scope.boxes = scope.boxes.filter(function (box) {
              return box.id !== boxToRemove.id;
            });
            generateQueryString();
            if (callback) {
              callback();
            }
            evaluateCloneButtonVisible();
          }, 50);
        };
        scope.editBox = function (box) {
          scope.removeBox(box, function () {
            scope.$broadcast('FilterBoxEditor:edit', box);
          });
        };
        scope.isStringType = function (value) {
          return typeof value === 'string';
        };
        scope.getFieldLabel = function (field) {
          return PAGINATED_TABLE_COLUMN_LABELS[field] || field.charAt(0).toUpperCase() + field.slice(1);
        };
        var generateQueryString = function () {
          var boxesEnabledCount = 0;
          angular.forEach(scope.boxes, function (box) {
            if (box.enabled) {
              boxesEnabledCount += 1;
            }
          });
          if (scope.boxes.length > 0 && boxesEnabledCount > 0) {
            var queryParts = [];
            angular.forEach(scope.boxes, function (box) {
              if (box.enabled) {
                queryParts.push(box.buildQueryString());
              }
            });
            scope.$emit('TextQuery:changed:relay', queryParts.join(' AND '));
          } else {
            scope.$emit('TextQuery:changed:relay', '*:*');
          }
        };
        scope.generateQueryString = generateQueryString;
        scope.shorten = function (text) {
          return Utils.shorten(text);
        }
      }
    };
  }

})();

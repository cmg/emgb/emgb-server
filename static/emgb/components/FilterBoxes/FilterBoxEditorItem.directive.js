/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.FilterBoxes')
    .directive('filterBoxEditorItem', FilterBoxEditorItem);

  function FilterBoxEditorItem($rootScope, ElasticSearch, $timeout, AUTOCOMPLETE_ENABLED, AUTOCOMPLETE_MAX_ITEMS) {
    return {
      scope: {
        dataset: '=dataset',
        filter: '=filter'
      },
      restrict: "A",
      link: function (scope, element, attrs) {
        scope.awesomplete = new Awesomplete(element[0], {
          minChars: 1,
          maxItems: AUTOCOMPLETE_MAX_ITEMS,
          autoFirst: true,
          filter: Awesomplete.FILTER_STARTSWITH,
          replace: function (text) {
            $timeout(function () {
              if (text !== undefined) {
                scope.filter.value.single = text.value;
              }
            }, 0);
          },
          sort: undefined
        });
        var autocompleteGeneField = function () {
          if (scope.filter.field !== '' && scope.filter.value.single !== ''
            && scope.filter.field !== undefined && scope.filter.value.single !== undefined
            && scope.$parent.getFieldType(scope.filter.field) === 'keyword'
            && scope.filter.field !== 'amino' && scope.filter.field !== 'nucleotide') {
            $rootScope.startSpinner();
            ElasticSearch.getAutocompletionForGeneField(scope.dataset.name, scope.filter.field, scope.filter.value.single, AUTOCOMPLETE_MAX_ITEMS)
              .then(function (resp) {
                var suggestions = [];
                angular.forEach(resp.aggregations.ac.buckets, function (bucket) {
                  suggestions.push(bucket.key);
                });
                if (suggestions.length === 1 && suggestions[0] === scope.filter.value.single) {
                  suggestions = [];
                }
                scope.awesomplete.list = suggestions;
                scope.awesomplete.evaluate();
                if (resp.aggregations.ac.sum_other_doc_count > 0) {
                  var ellipsis = document.createElement('li');
                  ellipsis.innerHTML = '...';
                  ellipsis.setAttribute("class", "autocomplete-ellipsis");
                  scope.awesomplete.ul.appendChild(ellipsis);
                }
              }, function (err) {
                console.trace(err.message);
              })
              .finally(function () {
                $rootScope.stopSpinner();
              });
          }
        };
        scope.$watch(angular.bind(scope.filter, function () {
          return scope.filter.value.single;
        }), function (newValue, oldValue) {
          if (AUTOCOMPLETE_ENABLED && newValue !== oldValue) {
            $timeout(function () {
              autocompleteGeneField();
            }, 0);
          }
        });
      }
    };
  }

})
();

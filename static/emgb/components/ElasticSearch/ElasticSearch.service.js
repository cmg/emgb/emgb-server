/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.ElasticSearch')
    .service('ElasticSearch', ElasticSearch);

  function ElasticSearch(ElasticSearchRESTClient) {
    var elasticSearch = this;

    elasticSearch.getAliases = getAliases;
    elasticSearch.getPage = getPage;
    elasticSearch.getMapping = getMapping;
    elasticSearch.getGene = getGene;
    elasticSearch.getDatasetGeneCounts = getDatasetGeneCounts;
    elasticSearch.getDatasetContigStats = getDatasetContigStats;
    elasticSearch.getPhyloAggregation = getPhyloAggregation;
    elasticSearch.getGoTermsAggregation = getGoTermsAggregation;
    elasticSearch.getPathwayList = getPathwayList;
    elasticSearch.getPathway = getPathway;
    elasticSearch.getPathwayDistribution = getPathwayDistribution;
    elasticSearch.getPathwayGeneDistribution = getPathwayGeneDistribution;
    elasticSearch.getGeneCountLabels = getGeneCountLabels;
    elasticSearch.getGeneCountSumsForPhyloLevel = getGeneCountSumsForPhyloLevel;
    elasticSearch.getNeighbors = getNeighbors;
    elasticSearch.resolveCartDataset = resolveCartDataset;
    elasticSearch.getContigsPage = getContigsPage;
    elasticSearch.getContig = getContig;
    elasticSearch.getContigsGenes = getContigsGenes;
    elasticSearch.searchAllForGeneIdOrAmino = searchAllForGeneIdOrAmino;
    elasticSearch.searchAllForGeneIds = searchAllForGeneIds;
    elasticSearch.getBinsAggregration = getBinsAggregration;
    elasticSearch.getBins = getBins;
    elasticSearch.getDatasetBinCounts = getDatasetBinCounts;
    elasticSearch.getAutocompletionForGeneField = getAutocompletionForGeneField;
    elasticSearch.getEcCount = getEcCount;

    function getAliases() {
      return ElasticSearchRESTClient.indices.getAlias();
    }

    function getPage(index, from, size, includedColumns, sort, query) {
      var sourceInclude = angular.copy(includedColumns);
      if (sourceInclude.indexOf('geneid') === -1) {
        sourceInclude.push('geneid');
      }
      if (sourceInclude.indexOf('amino') === -1) { // used for BLAST
        sourceInclude.push('amino');
      }
      sourceInclude.push('contigid'); // needed for getNeighbors()
      sourceInclude.push('start'); // needed for getNeighbors()
      sourceInclude.push('stop'); // needed for getNeighbors()
      sourceInclude.push('strand'); // needed for getNeighbors()
      return ElasticSearchRESTClient
        .search({
          index: index,
          type: 'gene',
          _sourceInclude: sourceInclude,
          body: {
            from: from,
            size: size,
            sort: sort,
            query: query
          }
        });
    }

    function getMapping(index) {
      var doc = {};
      if (index) {
        doc['index'] = index;
      }
      return ElasticSearchRESTClient.indices
        .getMapping(doc);
    }

    function getGene(dataset, geneid, columns) {
      return ElasticSearchRESTClient
        .search({
          index: dataset,
          type: 'gene',
          size: 1,
          _source: columns,
          body: {
            query: {
              bool: {
                filter: {
                  term: {
                    'geneid': geneid
                  }
                }
              }
            }
          }
        });
    }

    function getDatasetGeneCounts(datasets, nonPartialOnly) {
      var body = [];
      angular.forEach(datasets, function (dataset) {
        body.push({
          index: dataset,
          type: 'gene'
        });
        if (nonPartialOnly) {
          body.push({
            query: {
              bool: {
                filter: {
                  term: {
                    'partial': 'no'
                  }
                }
              }
            },
            size: 0
          });
        } else {
          body.push({
            query: {
              match_all: {}
            },
            size: 0
          });
        }
      });
      return ElasticSearchRESTClient
        .msearch({
          body: body
        });
    }

    function getDatasetContigStats(datasets) {
      var body = [];
      angular.forEach(datasets, function (dataset) {
        body.push({
          index: dataset,
          type: 'contig'
        });
        body.push({
          size: 0,
          query: {
            match_all: {}
          },
          aggs: {
            assemblySize: {
              sum: {
                field: 'length'
              }
            },
            lengths: {
              histogram: {
                field: "length",
                interval: 100,
                offset: 200,
                min_doc_count: 1
              }
            }
          }
        });
      });
      return ElasticSearchRESTClient
        .msearch({
          body: body
        });
    }

    function getPhyloAggregation(index, field, hierarchyFilter, textQuery) {
      if (!textQuery) {
        textQuery = {query_string: {query: "*:*"}};
      }
      return ElasticSearchRESTClient
        .search({
          index: index,
          type: 'gene',
          size: 0,
          body: {
            query: {
              bool: {
                filter: {
                  bool: {
                    must: [
                      textQuery,
                      {
                        bool: {
                          filter: hierarchyFilter
                        }
                      }
                    ]
                  }
                }
              }
            },
            aggs: {
              phylo: {
                terms: {
                  field: field,
                  size: 500
                }
              }
            }
          }
        });
    }

    function getGoTermsAggregation(index, boolQuery) {
      return ElasticSearchRESTClient
        .search({
          index: index,
          type: 'gene',
          size: 0,
          body: {
            query: boolQuery,
            aggs: {
              goterms: {
                nested: {
                  path: 'gos'
                },
                aggs: {
                  goids: {
                    terms: {
                      field: 'gos.goid',
                      size: 600
                    },
                    aggs: {
                      dedup: {
                        reverse_nested: {}
                      }
                    }
                  },
                  lineagegoids: {
                    terms: {
                      field: 'gos.golineage',
                      size: 600
                    },
                    aggs: {
                      dedup: {
                        reverse_nested: {}
                      }
                    }
                  }
                }
              }
            }
          }
        });
    }

    function getPathwayList(index) {
      return ElasticSearchRESTClient
        .search({
          index: index,
          type: 'pathway',
          size: 10000,
          _sourceInclude: ['pathid', 'title', 'group', 'subgroup'],
          body: {
            query: {
              match_all: {}
            },
            sort: [
              {'pathid': 'asc'}
            ]
          }
        });
    }

    function getPathway(index, pathId) {
      return ElasticSearchRESTClient
        .search({
          index: index,
          type: 'pathway',
          size: 1,
          body: {
            query: {
              bool: {
                filter: {
                  term: {
                    'pathid': 'map' + pathId
                  }
                }
              }
            }
          }
        });
    }

    function getPathwayDistribution(index, boolQuery) {
      return ElasticSearchRESTClient
        .search({
          index: index,
          type: 'gene',
          size: 0,
          body: {
            query: boolQuery,
            aggs: {
              pathwayDistribution: {
                terms: {
                  field: 'pathways.pathid',
                  size: 10000
                }
              }
            }
          }
        });
    }

    function getPathwayGeneDistribution(index, pathId, boolQuery) {
      return ElasticSearchRESTClient
        .search({
          index: index,
          type: 'gene',
          size: 0,
          body: {
            query: {
              bool: {
                filter: {
                  bool: {
                    must: [
                      boolQuery,
                      {
                        bool: {
                          filter: {
                            nested: {
                              path: 'pathways',
                              query: {
                                bool: {
                                  filter: {
                                    term: {
                                      'pathways.pathid': 'path:ko' + pathId
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    ]
                  }
                }
              }
            },
            aggs: {
              pathwaykos: {
                terms: {
                  field: 'ko',
                  size: 10000
                }
              }
            }
          }
        });
    }

    function getGeneCountLabels(index) {
      return ElasticSearchRESTClient
        .search({
          index: index,
          type: 'gene',
          size: 0,
          body: {
            aggs: {
              'countLabels': {
                terms: {
                  field: 'count.label',
                  order: {
                    _term: 'asc'
                  },
                  size: 100
                }
              }
            }
          }
        });
    }

    function getGeneCountSumsForPhyloLevel(index, phyloLevel, countLabelList) {
      var query = {
        index: index,
        type: 'gene',
        size: 0,
        timeout: '300s',
        requestTimeout: 300000,
        body: {
          aggs: {}
        }
      };
      angular.forEach(countLabelList, function (countLabel) {
        query.body.aggs['phyloLevel_' + countLabel] = {
          terms: {
            field: phyloLevel,
            size: 5000,
            collect_mode: 'breadth_first',
            order: {
              _term: "asc"
            }
          },
          aggs: {
            'insideCount': {
              nested: {
                path: 'count'
              },
              aggs: {
                'byCountLabel': {
                  filter: {
                    term: {
                      'count.label': countLabel
                    }
                  },
                  aggs: {
                    'geneCountSum': {
                      sum: {
                        field: 'count.count'
                      }
                    }
                  }
                }
              }
            }
          }
        };
        query.body.aggs['insideCount_' + countLabel] = {
          nested: {
            path: 'count'
          },
          aggs: {
            'byCountLabel': {
              filter: {
                term: {
                  'count.label': countLabel
                }
              },
              aggs: {
                'overallCountSum': {
                  sum: {
                    field: 'count.count'
                  }
                }
              }
            }
          }
        };
      });
      return ElasticSearchRESTClient
        .search(query);
    }

    function getNeighbors(index, hits, range) {
      var body = [];
      var sourceInclude = ['geneid', 'start', 'stop', 'strand', 'salltitles', 'pfams'];
      angular.forEach(hits, function (hit) {
        var gene = hit._source;
        body.push({});
        body.push({
          query: {
            bool: {
              filter: {
                bool: {
                  must: [
                    {
                      bool: {
                        filter: {
                          term: {
                            'contigid': gene.contigid
                          }
                        }
                      }
                    },
                    {
                      bool: {
                        filter: {
                          range: {
                            'stop': {
                              lt: gene.stop  //changed from gene.start to also include overlapping neighbors
                            }
                          }
                        }
                      }
                    }
                  ]
                }
              }
            }
          },
          sort: [
            {'stop': 'desc'}
          ],
          size: range,
          _source: sourceInclude
        });
        body.push({});
        body.push({
          query: {
            bool: {
              filter: {
                bool: {
                  must: [
                    {
                      bool: {
                        filter: {
                          term: {
                            'contigid': gene.contigid
                          }
                        }
                      }
                    },
                    {
                      bool: {
                        filter: {
                          range: {
                            'start': {
                              gt: gene.start  //changed from gene.stop to also include overlapping neighbors
                            }
                          }
                        }
                      }
                    }
                  ]
                }
              }
            }
          },
          sort: [
            {'start': 'asc'}
          ],
          size: range,
          _source: sourceInclude
        });
      });
      return ElasticSearchRESTClient
        .msearch({
          index: index,
          type: 'gene',
          body: body
        });
    }

    function resolveCartDataset(dataset, geneids, columns) {
      return ElasticSearchRESTClient
        .search({
          index: dataset,
          type: 'gene',
          size: 10000,
          _source: columns,
          body: {
            query: {
              bool: {
                filter: {
                  terms: {
                    'geneid': geneids
                  }
                }
              }
            }
          }
        });
    }

    function getContigsPage(dataset, gene_query, after, pageSize, withNucleotide, showVirSorterHitsOnly, showOnlyContigsLongerThan, longerThan, showOnlyEmptyContigs) {
      var fields = ['id', 'length', 'virsorter'];
      if (withNucleotide) {
        fields.push('nucleotide');
      }

      if (showOnlyEmptyContigs) {
        gene_query = {match_all: {}};
      }

      var has_child = {
        type: 'gene',
        query: gene_query,
        score_mode: 'sum',
        inner_hits: {
          _source: false
        }
      };

      var query = {
        bool: {
          must: [
            {
              has_child: has_child
            }
          ]
        }
      };
      if (showOnlyEmptyContigs) {
        query = {
          bool: {
            must_not: [
              {
                has_child: has_child
              }
            ]
          }
        };
      }

      if (showVirSorterHitsOnly || showOnlyContigsLongerThan) {
        if (showOnlyEmptyContigs) {
          query.bool = {
            must_not: [
              {
                has_child: has_child
              }
            ],
            should: [
              {
                bool: {
                  filter: []
                }
              }
            ],
            minimum_should_match: 1
          }
        } else {
          query.bool = {
            should: [
              {
                bool: {
                  filter: []
                }
              },
              {
                has_child: has_child
              }
            ],
            minimum_should_match: 2
          }
        }
      }

      if (showVirSorterHitsOnly) {
        query.bool.should[0].bool.filter.push(
          {
            exists: {
              field: 'virsorter'
            }
          }
        );
      }

      if (showOnlyContigsLongerThan) {
        query.bool.should[0].bool.filter.push(
          {
            range: {
              'length': {
                gt: longerThan
              }
            }
          }
        );
      }

      var body = {
        query: query,
        track_scores: true,
        sort: [
          {"_score": "desc", "length": "desc"}
        ]
      };
      if (after) {
        body['search_after'] = after;
      }
      return ElasticSearchRESTClient
        .search({
          index: dataset,
          type: 'contig',
          size: pageSize,
          _source: fields,
          body: body
        });
    }

    function getContig(dataset, contigId, withNucleotide, withCoverage) {
      var fields = ['id', 'length', 'virsorter'];
      if (withNucleotide) {
        fields.push('nucleotide');
      }
      if (withCoverage) {
        fields.push('coverage');
      }
      return ElasticSearchRESTClient
        .search({
          index: dataset,
          type: 'contig',
          size: 1,
          _source: fields,
          body: {
            query: {
              bool: {
                filter: {
                  term: {
                    'id': contigId
                  }
                }
              }
            }
          }
        });
    }

    function getContigsGenes(dataset, contigIds, extraFields) {
      var fieldsNeededForContigVis = ['geneid', 'start', 'stop', 'strand', 'salltitles', 'ko'];
      var excludedFields = ['nucleotide', 'amino'];
      var sourceFields = _.without(_.union(fieldsNeededForContigVis, extraFields), excludedFields);
      return ElasticSearchRESTClient
        .search({
          index: dataset,
          type: 'gene',
          size: 10000,
          _source: sourceFields,
          body: {
            query: {
              has_parent: {
                parent_type: 'contig',
                query: {
                  bool: {
                    filter: {
                      terms: {
                        'id': contigIds
                      }
                    }
                  }
                }
              }
            }
          }
        });
    }

    function searchAllForGeneIdOrAmino(input) {
      input = input.trim().replace(/\n/g, '');
      return ElasticSearchRESTClient
        .search({
          type: 'gene',
          size: 30,
          _source: ['geneid'],
          body: {
            query: {
              bool: {
                filter: {
                  bool: {
                    should: [
                      {
                        term: {
                          geneid: input
                        }
                      },
                      {
                        term: {
                          amino: input
                        }
                      }
                    ],
                    minimum_should_match: 1
                  }
                }
              }
            }
          }
        });
    }

    function searchAllForGeneIds(geneids, columns) {
      var q = {
        type: 'gene',
        size: 1000,
        body: {
          query: {
            bool: {
              filter: {
                terms: {
                  'geneid': geneids
                }
              }
            }
          }
        }
      };
      if (columns !== []) {
        q['_source'] = columns;
      }
      return ElasticSearchRESTClient
        .search(q);
    }

    function getBinsAggregration(index, boolQuery) {
      return ElasticSearchRESTClient
        .search({
          index: index,
          type: 'gene',
          size: 0,
          body: {
            query: boolQuery,
            aggs: {
              binsAggregration: {
                nested: {
                  path: 'binning'
                },
                aggs: {
                  bins: {
                    terms: {
                      field: 'binning.bins',
                      size: 10000
                    }
                  }
                }
              }
            }
          }
        });
    }

    function getBins(index) {
      return ElasticSearchRESTClient
        .search({
          index: index,
          type: 'bin',
          size: 10000,
          body: {
            query: {
              match_all: {}
            }
          }
        });
    }

    function getDatasetBinCounts(datasets, minCompleteness, maxContamination) {
      let body = [];
      angular.forEach(datasets, function (dataset) {
        body.push({
          index: dataset,
          type: 'bin'
        });
        body.push({
          query: {
            bool: {
              filter: {
                bool: {
                  must: [
                    {
                      bool: {
                        filter: {
                          range: {
                            'contamination': {
                              lt: maxContamination
                            }
                          }
                        }
                      }
                    },
                    {
                      bool: {
                        filter: {
                          range: {
                            'completeness': {
                              gt: minCompleteness
                            }
                          }
                        }
                      }
                    }
                  ]
                }
              }
            }
          },
          size: 0
        });
      });
      return ElasticSearchRESTClient
        .msearch({
          body: body
        });
    }

    function getAutocompletionForGeneField(index, field, prefix, limit) {
      var prefixFilter = {};
      prefixFilter[field] = prefix;
      return ElasticSearchRESTClient
        .search({
          index: index,
          type: 'gene',
          size: 0,
          body: {
            query: {
              bool: {
                filter: {
                  prefix: prefixFilter
                }
              }
            },
            aggs: {
              'ac': {
                terms: {
                  field: field,
                  size: limit,
                  order: {
                    _term: 'asc'
                  }
                }
              }
            }
          }
        });
    }

    function getEcCount(index, ecPrefix, filterBin) {
      var filter = {};
      if (ecPrefix[ecPrefix.length - 1] === '.') {
        filter['prefix'] = {
          'ecs': 'ec:' + ecPrefix
        };
      } else {
        filter['term'] = {
          'ecs': 'ec:' + ecPrefix
        };
      }
      var query = {
        filter: filter
      };
      if (filterBin !== undefined) {
        query = {
          must: [
            {
              bool: {
                filter: filter
              }
            },
            {
              bool: {
                filter: {
                  nested: {
                    path: 'binning',
                    query: {
                      bool: {
                        filter: {
                          term: {
                            'binning.bins': filterBin
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          ]
        }
      }
      return ElasticSearchRESTClient
        .search({
          index: index,
          type: 'gene',
          size: 0,
          body: {
            query: {
              bool: query
            }
          }
        });
    }
  }
})();

/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.ElasticSearch')
    .service('ElasticSearchRESTClient', ElasticSearchRESTClient);

  function ElasticSearchRESTClient(esFactory, ES_CLIENT_SETTINGS) {
    return esFactory(ES_CLIENT_SETTINGS);
  }

})();

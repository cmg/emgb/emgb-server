/* global angular, createjs */
(function () {
  'use strict';
  angular
    .module('emgb2.PathwayOverview')
    .directive('pathway', Pathway);

  function Pathway(ElasticSearch, $rootScope, BASE64_PNG_PREFIX, FilterBox) {
    return {
      scope: {},
      restrict: "A",
      require: '^pathwayOverview',
      link: function (scope, element, attrs, pathwayOverview) {

        var stage = new createjs.Stage(element[0]);
        stage.enableMouseOver();

        scope.$watch(angular.bind(pathwayOverview, function () {
          return pathwayOverview.selectedPathway;
        }), function (pathway) {
          highlight();
        }, true);

        if (!pathwayOverview.compare) {
          scope.$watchCollection(angular.bind(pathwayOverview, function () {
            return pathwayOverview.data;
          }), function (newValue, oldValue) {
            if (newValue && newValue.length > 0) {
              highlight();
            }
          });
        }

        if (pathwayOverview.compare) {
          scope.$on('DatasetTabs:changed', function (event, tabLabel) {
            if (tabLabel === 'compare-tab') {
              highlight();
            }
          });
        }

        function highlight() {
          var pathway = pathwayOverview.selectedPathway;
          if (pathway) {
            var img = new Image();
            img.onload = function () {
              stage.removeAllChildren();
              pathwayOverview.loadingPathway++;
              $rootScope.startSpinner();
              scope.$emit('DetailTabs:loading', 'pathway-overview', true);
              var numberOfDatasets = pathwayOverview.data.length;

              var background = new createjs.Shape();
              stage.addChild(background);
              background.graphics
                .beginFill('#ffffff')
                .drawRect(0, 0, pathway.w, pathway.h);

              var pathwayBitmapContainer = new createjs.Container();
              pathwayBitmapContainer.addChild(new createjs.Bitmap(img));
              stage.addChild(pathwayBitmapContainer);
              pathwayBitmapContainer.y = pathwayOverview.data.length * 26 + 10;
              var highlightingContainer = new createjs.Container();
              stage.addChild(highlightingContainer);
              highlightingContainer.y = pathwayOverview.data.length * 26 + 10;
              var legendContainer = new createjs.Container();
              stage.addChild(legendContainer);
              var colors = ['rgba(61, 133, 198, 0.6)', 'rgba(230, 145, 56, 0.6)', 'rgba(106, 168, 79, 0.6)', 'rgba(103, 78, 167, 0.6)', 'rgba(166, 77, 121, 0.6)'];
              angular.forEach(pathwayOverview.data, function (dataset, m) {
                var legendBox = new createjs.Shape();
                legendBox.graphics
                  .beginFill(colors[m % 5])
                  .drawRect(60, m * 26 + 8, 15, 15);
                legendBox.graphics.setStrokeStyle(1.5, "butt").beginStroke("#000").moveTo(53, m * 26 + 9).lineTo(65, m * 26 + 9);
                legendBox.graphics.moveTo(53, m * 26 + 9 + 13).lineTo(65, m * 26 + 9 + 13).endStroke();
                legendContainer.addChild(legendBox);
                var legendLabel = new createjs.Text(dataset.tabLabel ? dataset.tabLabel : dataset.name, "14px Arial", "#000");
                legendContainer.addChild(legendLabel);
                legendLabel.x = 80;
                legendLabel.y = m * 26 + 10;
                var legendMinGeneCount = new createjs.Text("0", "10px Arial", "#000");
                legendContainer.addChild(legendMinGeneCount);
                legendMinGeneCount.x = 50 - legendMinGeneCount.getMeasuredWidth();
                legendMinGeneCount.y = m * 26 + 6 + 11;
                ElasticSearch.getPathwayGeneDistribution(dataset.name, pathway.pathid.substring(3), dataset.query)
                  .then(function (resp) {
                    var boxContainer = new createjs.Container();
                    highlightingContainer.addChild(boxContainer);
                    var maxGeneCount = 0;
                    rectLoop: for (var i = 0; i < pathway.rects.length; i++) {
                      var geneCountForRect = 0;
                      var koMatchListForRect = [];
                      bucketLoop: for (var j = 0; j < resp.aggregations.pathwaykos.buckets.length; j++) {
                        koOfRectLoop: for (var k = 0; k < pathway.rects[i].kos.length; k++) {
                          if (resp.aggregations.pathwaykos.buckets[j].key === 'ko:' + pathway.rects[i].kos[k]) {
                            geneCountForRect += resp.aggregations.pathwaykos.buckets[j].doc_count;
                            koMatchListForRect.push(resp.aggregations.pathwaykos.buckets[j].key);
                          }
                        }
                      }
                      if (geneCountForRect > 0) {
                        maxGeneCount = Math.max(maxGeneCount, geneCountForRect);
                        var box = new createjs.Shape();
                        box.graphics
                          .beginFill('rgba(255,255,255,0.01)')
                          .drawRect(pathway.rects[i].x1 + 0.5, pathway.rects[i].y1 + 0.5, pathway.rects[i].x2 - pathway.rects[i].x1, pathway.rects[i].y2 - pathway.rects[i].y1);
                        box.metadata = {
                          count: geneCountForRect,
                          kos: koMatchListForRect,
                          rect: pathway.rects[i]
                        };
                        box.cursor = "pointer";
                        box.on('click', function (evt) {
                          var filterBox = new FilterBox('KO filter');
                          for (var i = 0; i < this.metadata.kos.length; i++) {
                            filterBox.addFilter('ko', this.metadata.kos[i]);
                          }
                          scope.$emit('FilterBoxes:add:relay', filterBox);
                        });
                        boxContainer.addChild(box);
                      }
                    }

                    var legendMaxGeneCount = new createjs.Text(maxGeneCount, "10px Arial", "#000");
                    legendContainer.addChild(legendMaxGeneCount);
                    legendMaxGeneCount.x = 50 - legendMaxGeneCount.getMeasuredWidth();
                    legendMaxGeneCount.y = m * 26 + 6;

                    var countBoxContainer = new createjs.Container();
                    stage.addChild(countBoxContainer);
                    if (boxContainer.numChildren > 0) {
                      for (var i = 0; i < boxContainer.numChildren; i++) {
                        var box = boxContainer.getChildAt(i);
                        var geneCountRatio = box.metadata.count / maxGeneCount;
                        var x = box.metadata.rect.x1 + ((box.metadata.rect.x2 - box.metadata.rect.x1) / numberOfDatasets) * m;
                        var y = box.metadata.rect.y1 + (box.metadata.rect.y2 - box.metadata.rect.y1) * (1 - geneCountRatio);
                        var w = (box.metadata.rect.x2 - box.metadata.rect.x1) / numberOfDatasets;
                        var h = (box.metadata.rect.y2 - box.metadata.rect.y1) * geneCountRatio;
                        box.graphics
                          .beginFill(colors[m % 5])
                          .drawRect(x, y, w, h);
                      }
                    }
                    stage.update();
                  })
                  .finally(function () {
                    scope.$emit('DetailTabs:loading', 'pathway-overview', false);
                    $rootScope.stopSpinner();
                    pathwayOverview.loadingPathway--;
                  });
              });
            };
            img.src = BASE64_PNG_PREFIX + pathway.img;
          }
        }
      }
    };
  }

})();

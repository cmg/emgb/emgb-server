/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.GoStats')
    .controller('PathwayOverviewHelper', PathwayOverviewHelper);

  function PathwayOverviewHelper($scope) {
    var pathwayOverviewHelper = this;
    var boolQueryBuffer;
    var boolQueryStale = true;

    pathwayOverviewHelper.datasets = [];

    $scope.$on('ResultFilters:changed', function (event, boolQuery) {
      boolQueryBuffer = boolQuery;
      boolQueryStale = true;
      if ($scope.detailTabs.activeTabLabel === 'pathway-overview') {
        applyBoolQueryBuffer();
      }
    });

    $scope.$on('DetailTabs:changed', function (event, tabLabel) {
      if (tabLabel === 'pathway-overview') {
        applyBoolQueryBuffer();
      }
    });

    function applyBoolQueryBuffer() {
      if (boolQueryStale) {
        pathwayOverviewHelper.datasets = [{name: $scope.dataset.name, query: boolQueryBuffer}];
        boolQueryStale = false;
      }
    }
  }

})();

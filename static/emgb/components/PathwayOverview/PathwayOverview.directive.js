/* global angular, createjs */
(function () {
  'use strict';
  angular
    .module('emgb2.PathwayOverview')
    .directive('pathwayOverview', PathwayOverview);

  function PathwayOverview(ElasticSearch, $rootScope, FilterBox) {
    return {
      scope: {
        data: '=',
        compare: '='
      },
      restrict: "E",
      templateUrl: 'components/PathwayOverview/pathway-overview.html',
      controllerAs: 'pathwayOverview',
      bindToController: true,
      controller: function ($scope) {
        var pathwayOverview = this;

        pathwayOverview.pathways = [];
        pathwayOverview.selectedPathway;
        pathwayOverview.idsLoaded = false;
        pathwayOverview.imageVisible = false;
        pathwayOverview.loadingList = 0;
        pathwayOverview.loadingPathway = 0;

        pathwayOverview.loadPathway = loadPathway;
        pathwayOverview.addPathwayFilterBox = addPathwayFilterBox;
        pathwayOverview.hasGeneHits = hasGeneHits;

        function loadPathwayList() {
          pathwayOverview.idsLoaded = true;
          pathwayOverview.loadingList++;
          $rootScope.startSpinner();
          $scope.$emit('DetailTabs:loading', 'pathway-overview', true);
          ElasticSearch.getPathwayDistribution(pathwayOverview.data[0].name, pathwayOverview.data[0].query)
            .then(function (distrResp) {
              ElasticSearch.getPathwayList(pathwayOverview.data[0].name)
                .then(function (listResp) {
                  pathwayOverview.pathways = [];
                  angular.forEach(listResp.hits.hits, function (pathway) {
                    var currentPathwayData = pathway._source;
                    currentPathwayData.pathid = currentPathwayData.pathid.substring(3);
                    currentPathwayData.geneHits = 0;
                    angular.forEach(distrResp.aggregations.pathwayDistribution.buckets, function (bucket) {
                      if (bucket.key.substring(7) === currentPathwayData.pathid && bucket.doc_count > 0) {
                        currentPathwayData.geneHits = bucket.doc_count;
                      }
                    });
                    pathwayOverview.pathways.push(currentPathwayData);
                  });
                }, function (err) {
                  console.trace(err.message);
                })
                .finally(function () {
                  $scope.$emit('DetailTabs:loading', 'pathway-overview', false);
                  $rootScope.stopSpinner();
                  pathwayOverview.loadingList--;
                });
            }, function (err) {
              console.trace(err.message);
            });
        }

        function loadPathway(pathwayId) {
          if (pathwayId) {
            pathwayOverview.loadingPathway++;
            $rootScope.startSpinner();
            ElasticSearch.getPathway(pathwayOverview.data[0].name, pathwayId)
              .then(function (resp) {
                pathwayOverview.selectedPathway = resp.hits.hits[0]._source;
                pathwayOverview.imageVisible = true;
              }, function (err) {
                console.trace(err.message);
              })
              .finally(function () {
                $rootScope.stopSpinner();
                pathwayOverview.loadingPathway--;
              });
          }
        }

        function addPathwayFilterBox(pathwayId, pathwayTitle) {
          var filterBox = new FilterBox('Pathway: ' + pathwayTitle);
          filterBox.addFilter('pathways.pathid', 'path:ko' + pathwayId);
          $scope.$emit('FilterBoxes:add:relay', filterBox);
        }

        function hasGeneHits(pathway) {
          if (pathwayOverview.compare) {
            return true;
          }
          return pathway.geneHits > 0;
        }

        var onTabChanged = function (event, tabLabel) {
          if (tabLabel === 'compare-tab') {
            if (!pathwayOverview.idsLoaded) {
              loadPathwayList();
            }
          }
        };

        if (!pathwayOverview.compare) {
          $scope.$watchCollection(angular.bind(pathwayOverview, function () {
            return pathwayOverview.data;
          }), function (newValue, oldValue) {
            if (newValue && newValue.length > 0) {
              loadPathwayList();
            }
          });
        }

        if (pathwayOverview.compare) {
          $scope.$on('DatasetTabs:changed', onTabChanged);
        } else {
          $scope.$on('DetailTabs:changed', onTabChanged);
        }

        $scope.$on('PathwayOverview:selectPathway', function (event, pathwayId) {
          loadPathway(pathwayId);
        });
      }
    };
  }

})();

/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.TargetDb')
    .directive("targetDbIndicator", TargetDbIndicator);

  function TargetDbIndicator($http, TARGETDB_SERVICE_URL, $sce) {
    return {
      restrict: 'E',
      template: '<div class="target-db-indicator alert alert-{{color}}"' +
        'uib-tooltip-html="tooltipHtml" tooltip-placement="top-right" tooltip-popup-delay="100">' +
        '<i ng-show="loading" class="fa fa-spinner fa-pulse"></i>' +
        '<span ng-show="!loading"><i class="fa fa-bullseye" style="margin-right:3px;"></i>{{msg}}</span></div>',
      scope: {
        amino: '=amino',
        datasetTabs: '=datasetTabs'
      },
      controller: function ($scope) {
        $scope.msg = "";
        $scope.loading = true;
        $scope.color = "info";
        $scope.tooltipHtml = $sce.trustAsHtml('Real-time BLAST search against already picked targets.');

        function analyzeHits(hits) {
          if (hits.length === 0) {
            $scope.msg = "no match";
            $scope.color = "success";
            $scope.tooltipHtml = $sce.trustAsHtml('This protein sequence is not similar to any already picked targets.');
          } else {
            var html = 'The target master list already contains <b>' + hits.length +
              '</b> similar protein sequence' + (hits.length > 1 ? 's' : '') + '.<br>' +
              '<u>Most similar one:</u><br>' +
              '<b>E-value:</b> ' + hits[0]['evalue'].toExponential(2) + '<br>' +
              '<b>Title:</b> ' + hits[0]['id'];
            $scope.msg = '' + hits[0]['evalue'].toExponential(2);
            $scope.color = "danger";
            $scope.tooltipHtml = $sce.trustAsHtml(html);
          }
        }

        if ($scope.datasetTabs && $scope.datasetTabs.targetDbUnavailable) {
          $scope.loading = false;
          $scope.msg = "offline";
          $scope.color = "warning";
        } else {
          $http(
            {
              method: 'POST',
              url: TARGETDB_SERVICE_URL + 'run_blast',
              headers: {
                'Accept': 'application/json'
              },
              data: {
                seq_type: 'p',
                query: $scope.amino,
                return_alignment: false
              }
            })
            .then(function (resp) {
              analyzeHits(resp.data.hits);
              $scope.loading = false;
            }, function (err) {
              $scope.loading = false;
              $scope.msg = "offline";
              $scope.color = "warning";
            })
            .finally(function () {
            });
        }
      }
    };
  }
})();
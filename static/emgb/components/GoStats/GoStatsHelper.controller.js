/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.GoStats')
    .controller('GoStatsHelper', GoStatsHelper);

  function GoStatsHelper($scope) {
    var goStatsHelper = this;
    var boolQueryBuffer;
    var boolQueryStale = true;

    goStatsHelper.datasets = [];

    $scope.$on('ResultFilters:changed', function (event, boolQuery) {
      boolQueryBuffer = boolQuery;
      boolQueryStale = true;
      if ($scope.detailTabs.activeTabLabel === 'go-stats') {
        applyBoolQueryBuffer();
      }
    });

    $scope.$on('DetailTabs:changed', function (event, tabLabel) {
      if (tabLabel === 'go-stats') {
        applyBoolQueryBuffer();
      }
    });

    function applyBoolQueryBuffer() {
      if (boolQueryStale) {
        goStatsHelper.datasets = [{name: $scope.dataset.name, query: boolQueryBuffer, total: $scope.dataset.total}];
        boolQueryStale = false;
      }
    }
  }

})();

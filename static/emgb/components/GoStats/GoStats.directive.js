/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.GoStats')
    .directive('goStats', GoStats);

  function GoStats(ElasticSearch, $rootScope, GO_STATS_CATEGORIES, FilterBox, Utils, $timeout) {
    return {
      scope: {
        data: '=',
        compare: '='
      },
      restrict: "E",
      templateUrl: 'components/GoStats/go-stats.html',
      controllerAs: 'goStats',
      bindToController: true,
      controller: function ($scope) {
        var goStats = this;

        goStats.barClicked = barClicked;
        goStats.loaded = false;
        goStats.loading = 0;
        goStats.config = {};
        var commonOptions = {
          chart: {
            type: "multiBarHorizontalChart",
            multibar: {
              dispatch: {
                elementClick: goStats.barClicked
              }
            },
            height: 500,
            margin: {
              top: 20,
              right: 30,
              bottom: 60,
              left: 240
            },
            clipEdge: true,
            duration: 300,
            stacked: false,
            showControls: false,
            xAxis: {
              axisLabel: ""
            },
            yAxis: {
              axisLabel: "% of genes",
              tickFormat: function (d) {
                return d;
              }
            },
            noData: ""
          },
          title: {
            enable: true
          }
        };
        goStats.charts = {
          biological_process: {
            data: [{key: "", values: []}],
            api: {},
            options: angular.copy(commonOptions)
          },
          cellular_component: {
            data: [{key: "", values: []}],
            api: {},
            options: angular.copy(commonOptions)
          },
          molecular_function: {
            data: [{key: "", values: []}],
            api: {},
            options: angular.copy(commonOptions)
          }
        };
        goStats.charts.biological_process.options.title.text = 'biological process';
        goStats.charts.biological_process.options.chart.xAxis.tickFormat = function (d) {
          return Utils.shorten(GO_STATS_CATEGORIES.biological_process[d].description);
        };
        goStats.charts.cellular_component.options.title.text = 'cellular component';
        goStats.charts.cellular_component.options.chart.xAxis.tickFormat = function (d) {
          return Utils.shorten(GO_STATS_CATEGORIES.cellular_component[d].description);
        };
        goStats.charts.molecular_function.options.title.text = 'molecular function';
        goStats.charts.molecular_function.options.chart.xAxis.tickFormat = function (d) {
          return Utils.shorten(GO_STATS_CATEGORIES.molecular_function[d].description);
        };

        function updateCharts() {
          if (goStats.compare) {
            angular.forEach(['biological_process', 'cellular_component', 'molecular_function'], function (namespace) {
              goStats.charts[namespace].data = [];
            });
          }
          angular.forEach(goStats.data, function (dataset, j) {
            if (goStats.compare) {
              angular.forEach(['biological_process', 'cellular_component', 'molecular_function'], function (namespace) {
                goStats.charts[namespace].data.push({
                  key: dataset.tabLabel ? dataset.tabLabel : dataset.name,
                  values: []
                });
              });
            }
            goStats.loading++;
            $rootScope.startSpinner();
            $scope.$emit('DetailTabs:loading', 'go-stats', true);
            ElasticSearch.getGoTermsAggregation(dataset.name, dataset.query)
              .then(function (resp) {
                var totalGeneCount = dataset.total;
                var goIds = resp.aggregations.goterms.goids.buckets;
                var lineageGoIds = resp.aggregations.goterms.lineagegoids.buckets;
                var docCount;
                angular.forEach(['biological_process', 'cellular_component', 'molecular_function'], function (namespace) {
                  var categoryCount = 0;
                  goStats.charts[namespace].data[j].values = [];
                  angular.forEach(GO_STATS_CATEGORIES[namespace], function (category, i) {
                    docCount = 0;
                    angular.forEach(goIds, function (goId) {
                      if (goId.key === category.id) {
                        docCount += goId.dedup.doc_count;
                      }
                    });
                    angular.forEach(lineageGoIds, function (goId) {
                      if (goId.key === category.id) {
                        docCount += goId.dedup.doc_count;
                      }
                    });
                    goStats.charts[namespace].data[j].key = dataset.tabLabel ? dataset.tabLabel : dataset.name;
                    goStats.charts[namespace].data[j].values.push({
                      x: i,
                      y: Math.round(docCount * 10000 / totalGeneCount) / 100,
                      goId: category.id,
                      goDesc: category.description
                    });
                    categoryCount++;
                  });
                  goStats.charts[namespace].options.chart.height = 150 + (16 * categoryCount * goStats.data.length) * (goStats.compare ? 0.5 : 1);
                });
                goStats.loaded = true;
              }, function (err) {
                console.trace(err.message);
              })
              .finally(function () {
                $scope.$emit('DetailTabs:loading', 'go-stats', false);
                $rootScope.stopSpinner();
                goStats.loading--;
              });
          });
        }

        var onTabChanged = function (event, tabLabel) {
          if (tabLabel === 'go-stats' || tabLabel === 'compare-tab') {
            if (goStats.compare) {
              updateCharts();
            } else {
              if (goStats.loaded) {
                goStats.charts.biological_process.api.update();
                goStats.charts.cellular_component.api.update();
                goStats.charts.molecular_function.api.update();
              } else {
                updateCharts();
              }
            }
          }
        };
        if (goStats.compare) {
          $scope.$on('DatasetTabs:changed', onTabChanged);
        } else {
          $scope.$on('DetailTabs:changed', onTabChanged);
          $scope.$on('PhyloTree:toggled', function (event, visible) {
            $timeout(function () {
              goStats.charts.biological_process.api.update();
              goStats.charts.cellular_component.api.update();
              goStats.charts.molecular_function.api.update();
            });
          });
        }

        if (!goStats.compare) {
          $scope.$watchCollection(angular.bind(goStats, function () {
            return goStats.data;
          }), function (newValue, oldValue) {
            if (newValue.length > 0) {
              updateCharts();
            }
          });
        }

        function barClicked(element) {
          var filterBox = new FilterBox('GO: ' + element.data.goDesc, 'OR');
          filterBox.addFilter('gos.goid', element.data.goId);
          filterBox.addFilter('gos.golineage', element.data.goId);
          $scope.$emit('FilterBoxes:add:relay', filterBox);
        }
      }
    };
  }

})();

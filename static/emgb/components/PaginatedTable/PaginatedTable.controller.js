/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.PaginatedTable')
    .controller('PaginatedTable', PaginatedTable);

  function PaginatedTable(ElasticSearch, $rootScope, $scope, $timeout, $uibModal, FilterBox, ExternalTools, Utils, PAGINATED_TABLE_COLUMN_LABELS, PAGINATED_TABLE_GENERATED_COLUMNS, PAGINATED_TABLE_HIDDEN_COLUMNS, PHYLO_TREE_RANK_LABELS, MODE) {
    var paginatedTable = this;

    paginatedTable.templates = {
      horizontal: 'components/PaginatedTable/paginated-table.horizontal.html',
      vertical: 'components/PaginatedTable/paginated-table.vertical.html'
    };
    paginatedTable.currentTemplate = 'vertical';
    paginatedTable.from = 0;
    paginatedTable.columnWidthPx = 300;
    paginatedTable.size = -1;
    paginatedTable.sortBy = {};
    paginatedTable.hits = {};
    paginatedTable.total = -1;
    paginatedTable.availableColumns = [];
    paginatedTable.sortable = [];
    paginatedTable.visibleColumns = ['salltitles', 'evalue', 'pbestclass', 'pbestlevel', 'neighbors', 'pident', 'partial'];
    paginatedTable.columnPresets = ['minimal', 'default', 'targeting', 'all'];
    paginatedTable.columnSelectorVisible = false;
    paginatedTable.externalTools = ExternalTools;
    paginatedTable.neighborRange = 3;
    paginatedTable.neighborsLoadingPromise;
    paginatedTable.cartDataCopy = {};
    paginatedTable.loading = false;
    paginatedTable.mode = MODE;
    paginatedTable.contigViewerHighlightSelection = 'pgenus';  // used for caching selection for contig viewer popup
    paginatedTable.contigViewerDisplayCoverage = false;  // used for caching user choice for contig viewer popup
    paginatedTable.contigViewerDisplayCoverageForName = '';  // used for caching user choice for contig viewer popup

    paginatedTable.fetch = fetch;
    paginatedTable.pageDown = pageDown;
    paginatedTable.hasPageDown = hasPageDown;
    paginatedTable.pageUp = pageUp;
    paginatedTable.hasPageUp = hasPageUp;
    paginatedTable.sort = sort;
    paginatedTable.getColumnLabel = getColumnLabel;
    paginatedTable.resizeTable = resizeTable;
    paginatedTable.rotate = rotate;
    paginatedTable.getRankLabel = getRankLabel;
    paginatedTable.format = format;
    paginatedTable.addValueFilterBox = addValueFilterBox;
    paginatedTable.addPresentFilterBox = addPresentFilterBox;
    paginatedTable.isFilterBoxSuitable = isFilterBoxSuitable;
    paginatedTable.switchToPathwayView = switchToPathwayView;
    paginatedTable.applyVisibleColumnsPreset = applyVisibleColumnsPreset;
    paginatedTable.addGeneToCart = addGeneToCart;
    paginatedTable.sortObjectArray = Utils.sortObjectArray;
    paginatedTable.sortObjectArrayByNumber = Utils.sortObjectArrayByNumber;
    paginatedTable.shorten = Utils.shorten;
    paginatedTable.isGeneInCart = isGeneInCart;
    paginatedTable.openContigPopup = openContigPopup;
    paginatedTable.openBlastHitsPopup = openBlastHitsPopup;

    var sizeBeforeRotation;
    var query = {"bool": {"filter": []}};
    var queryStale = false;

    function applyQuery() {
      if (queryStale) {
        paginatedTable.from = 0;
        if (paginatedTable.visibleColumns.length > 0) {
          fetch();
        }
        queryStale = false;
      }
    }

    $scope.$on('ResultFilters:changed', function (event, boolQuery) {
      query = boolQuery;
      queryStale = true;
      if ($scope.detailTabs.activeTabLabel === 'table') {
        applyQuery();
      }
    });

    $scope.$on('DetailTabs:changed', function (event, tabLabel) {
      if (tabLabel === 'table') {
        applyQuery();
      }
    });

    $scope.$on('DatasetTab:updateLayout', function () {
      resizeTable();
    });

    $scope.$on('DatasetTabs:changed', function (event, tabLabel) {
      resizeTable();
    });

    $scope.$on('PaginatedTable:openContigPopup', function (event, contigid, geneid) {
      openContigPopup(contigid, geneid);
    });

    $scope.$watchCollection(angular.bind(paginatedTable, function () {
      return paginatedTable.visibleColumns;
    }), function (newValue, oldValue) {
      if (paginatedTable.size === -1) {
        resizeTable();
        fetchColumns();
      } else {
        fetch();
      }
    });

    function fetchColumns() {
      $rootScope.startSpinner();
      paginatedTable.loading = true;
      $scope.$emit('DetailTabs:loading', 'paginated-table', true);
      ElasticSearch.getMapping($scope.dataset.name)
        .then(function (resp) {
          paginatedTable.availableColumns = Object.keys(resp[$scope.dataset.name].mappings.gene.properties);
          _.pullAll(paginatedTable.availableColumns, PAGINATED_TABLE_HIDDEN_COLUMNS);
          angular.forEach(paginatedTable.availableColumns, function (column) {
            paginatedTable.sortable[column] = resp[$scope.dataset.name].mappings.gene.properties[column].type !== 'nested'
              && resp[$scope.dataset.name].mappings.gene.properties[column].type !== 'text';
          });
          Array.prototype.push.apply(paginatedTable.availableColumns, PAGINATED_TABLE_GENERATED_COLUMNS);
          paginatedTable.availableColumns = paginatedTable.availableColumns.filter(function (availableColumn) {
            return paginatedTable.visibleColumns.indexOf(availableColumn) === -1;
          }).sort(function (a, b) {
            return getColumnLabel(a).localeCompare(getColumnLabel(b));
          });
        }, function (err) {
          console.trace(err.message);
        })
        .finally(function () {
          $scope.$emit('DetailTabs:loading', 'paginated-table', false);
          $rootScope.stopSpinner();
        });
    }

    function fetch(callback) {
      if (paginatedTable.neighborsLoadingPromise) {
        $timeout.cancel(paginatedTable.neighborsLoadingPromise);
      }
      $rootScope.startSpinner();
      paginatedTable.loading = true;
      $scope.$emit('DetailTabs:loading', 'paginated-table', true);
      var staticColumns = paginatedTable.visibleColumns.filter(function (val) {
        return PAGINATED_TABLE_GENERATED_COLUMNS.indexOf(val) === -1;
      });
      ElasticSearch.getPage($scope.dataset.name, paginatedTable.from, paginatedTable.size, staticColumns, paginatedTable.sortBy, query)
        .then(function (resp) {
          paginatedTable.hits = resp.hits.hits;
          paginatedTable.total = resp.hits.total;
          if (paginatedTable.visibleColumns.indexOf('neighbors') !== -1) {
            paginatedTable.neighborsLoadingPromise = $timeout(function () {
              attachNeighbors(paginatedTable.hits);
            }, 1000);
          }
        }, function (err) {
          console.trace(err.message);
        })
        .finally(function () {
          $scope.$emit('DetailTabs:loading', 'paginated-table', false);
          paginatedTable.loading = false;
          $rootScope.stopSpinner();
        })
        .then(callback || angular.noop);
    }

    function hasPageDown(num) {
      return paginatedTable.from - (paginatedTable.size * num) >= 0 || (paginatedTable.from > 1 && paginatedTable.from < paginatedTable.size * num);
    }

    function pageDown(num) {
      if (hasPageDown(num)) {
        paginatedTable.from = Math.max(0, paginatedTable.from - paginatedTable.size * num);
      }
      fetch();
    }

    function hasPageUp(num) {
      return (paginatedTable.from + (paginatedTable.size * num)) <= paginatedTable.total;
    }

    function pageUp(num) {
      if (hasPageUp(num)) {
        paginatedTable.from += paginatedTable.size * num;
      }
      fetch();
    }

    function sort(column) {
      if (paginatedTable.sortable[column]) {
        if (paginatedTable.sortBy[column] === 'desc') {
          paginatedTable.sortBy[column] = 'asc';
        } else if (paginatedTable.sortBy[column] === undefined) {
          paginatedTable.sortBy = {};
          paginatedTable.sortBy[column] = 'desc';
        } else {
          paginatedTable.sortBy = {};
        }
        fetch();
      }
    }

    function getColumnLabel(column) {
      return PAGINATED_TABLE_COLUMN_LABELS[column] || column.charAt(0).toUpperCase() + column.slice(1);
    }

    function resizeTable(event) {
      if ($scope.dataset.id === $scope.datasetTabs.activeTabIndex) {
        $timeout(function () {
          if (document.getElementById('paginated-table-container-' + $scope.dataset.id)) {
            var previousSize = paginatedTable.size;
            paginatedTable.size = Math.max(1, Math.round((document.getElementById('paginated-table-container-' + $scope.dataset.id).offsetWidth - paginatedTable.columnWidthPx) / paginatedTable.columnWidthPx));
            if (paginatedTable.size !== previousSize) {
              fetch();
            }
          }
        });
      }
    }

    function rotate() {
      if (paginatedTable.currentTemplate === 'vertical') {
        paginatedTable.currentTemplate = 'horizontal';
        sizeBeforeRotation = paginatedTable.size;
        paginatedTable.size = 10;
        fetch();
      } else if (paginatedTable.currentTemplate === 'horizontal') {
        paginatedTable.size = sizeBeforeRotation;
        fetch(function () {
          paginatedTable.currentTemplate = 'vertical';
        });
      }
    }

    function getRankLabel(columnName) {
      return PHYLO_TREE_RANK_LABELS[columnName];
    }

    function format(columnName, data) {
      var out = data;
      if (data) {
        if (columnName === 'evalue') {
          return data.toExponential(2);
        } else if (columnName === 'pident') {
          return data.toFixed(1);
        } else if (columnName === 'score') {
          return data.toFixed(2);
        } else if (columnName === 'sseqid') {
          return data.replace(/\|([a-z])/g, ' $&');
        } else if (columnName === 'gos') {
          out = ''; // render template instead
        } else if (columnName === 'pathways') {
          out = ''; // render template instead
        } else if (columnName === 'pfams') {
          out = ''; // render template instead
        } else if (columnName === 'x_pfam') {
          out = ''; // render template instead
        } else if (columnName === 'x_pdb') {
          out = ''; // render template instead
        } else if (columnName === 'x_3dm') {
          out = ''; // render template instead
        } else if (columnName === 'count') {
          out = ''; // render template instead
        } else if (columnName === 'blast') {
          out = ''; // render template instead
        } else if (columnName === 'binning') {
          out = '';
          for (var i = 0; i < data.length; i++) {
            out += data[i].label + ': ' + data[i].bins.join(', ') + '; ';
          }
        } else if (columnName === 'ecs') {
          out = data.filter(function (item, pos, self) {  // remove duplicate ecs
            return self.indexOf(item) == pos;
          }).join(', ');
        }
      }
      return out;
    }

    function addValueFilterBox(columnName, columnData, columnLabel) {
      var filterBox = new FilterBox(columnLabel);
      filterBox.addFilter(columnName, columnData);
      $scope.$emit('FilterBoxes:add:relay', filterBox);
    }

    function addPresentFilterBox(columnName, columnLabel) {
      var filterBox = new FilterBox(columnLabel);
      filterBox.addFilter(columnName, '', 'present');
      $scope.$emit('FilterBoxes:add:relay', filterBox);
    }

    function isFilterBoxSuitable(value) {
      return typeof value !== 'undefined' && typeof value !== 'object' && value !== '';
    }

    function switchToPathwayView(pathid) {
      $scope.$emit('DetailTabs:forceSelection', 'pathway-overview', pathid.substring(7));
    }

    function attachNeighbors(hits) {
      if (hits.length > 0) {
        ElasticSearch.getNeighbors($scope.dataset.name, hits, paginatedTable.neighborRange + 1)
          .then(function (neighborHits) {
            angular.forEach(hits, function (hit, i) {
              hit.leftNeighbors = neighborHits['responses'][i * 2]['hits']['hits'];
              hit.rightNeighbors = neighborHits['responses'][i * 2 + 1]['hits']['hits'];
              if (hit.leftNeighbors.length === paginatedTable.neighborRange + 1) {
                hit.leftNeighbors.pop();
                hit.hasMoreLeftNeighbors = true;
              }
              if (hit.rightNeighbors.length === paginatedTable.neighborRange + 1) {
                hit.rightNeighbors.pop();
                hit.hasMoreRightNeighbors = true;
              }
              hit.neighborsLoaded = true;
            });
          }, function (err) {
            console.trace(err.message);
          });
      }
    }

    function applyVisibleColumnsPreset(name) {
      paginatedTable.columnSelectorVisible = false;
      Array.prototype.push.apply(paginatedTable.availableColumns, paginatedTable.visibleColumns);
      if (name === 'minimal') {
        paginatedTable.visibleColumns = ['salltitles', 'evalue', 'pbestclass', 'pbestlevel'];
      } else if (name === 'default') {
        paginatedTable.visibleColumns = ['salltitles', 'evalue', 'pbestclass', 'pbestlevel', 'neighbors', 'pident', 'partial'];
      } else if (name === 'targeting') {
        paginatedTable.visibleColumns = ['salltitles', 'evalue', 'pbestclass', 'pbestlevel', 'pident', 'partial', 'neighbors', 'gos', 'pathways', 'ecs', 'pfams', 'x_pfam', 'x_pdb', 'x_3dm', 'x_prevalence', 'amino'];
      } else if (name === 'all') {
        paginatedTable.visibleColumns = paginatedTable.availableColumns;
        paginatedTable.availableColumns = [];
      }
      paginatedTable.availableColumns = paginatedTable.availableColumns.filter(function (availableColumn) {
        return paginatedTable.visibleColumns.indexOf(availableColumn) === -1;
      }).sort(function (a, b) {
        return getColumnLabel(a).localeCompare(getColumnLabel(b));
      });
    }

    function addGeneToCart(dataset, geneid) {
      $scope.$emit('Cart:add:relay', dataset, geneid);
    }

    $scope.$on('Cart:sendCopy', function (event, data) {
      paginatedTable.cartDataCopy = data;
    });

    $scope.$emit('Cart:requestCopy:relay');

    function isGeneInCart(dataset, geneid) {
      if (paginatedTable.cartDataCopy.genes) {
        if (paginatedTable.cartDataCopy.genes[dataset]) {
          if (paginatedTable.cartDataCopy.genes[dataset][geneid]) {
            return true;
          }
        }
      }
      return false;
    }

    function openContigPopup(contigid, geneid) {
      $uibModal.open({
        animation: false,
        backdrop: true, //'static',
        templateUrl: 'components/ContigViewer/contig-viewer.modal.html',
        controller: 'ContigViewerModalCtrl',
        controllerAs: 'cvModalCtrl',
        size: 'lg',
        resolve: {
          contigid: function () {
            return contigid;
          },
          geneid: function () {
            return geneid;
          },
          dataset: function () {
            return $scope.dataset;
          },
          pgTbl: function () {
            return paginatedTable;
          }
        }
      });
    }

    function openBlastHitsPopup(geneid) {
      $uibModal.open({
        animation: false,
        backdrop: true, //'static',
        templateUrl: 'components/BlastHits/blast-hits.modal.html',
        controller: 'BlastHitsModalCtrl',
        controllerAs: 'bhModalCtrl',
        size: 'lg',
        resolve: {
          geneid: function () {
            return geneid;
          },
          dataset: function () {
            return $scope.dataset;
          }
        }
      });
    }

    $timeout(function () {
      if ($scope.dataset.initialFilter) {
        $scope.$emit('FilterBoxes:add:relay', $scope.dataset.initialFilter);
      }
    });
    // $scope.$emit('TextQuery:changed:relay', '(geneid:"MHRB_Z1Z4AG_XHMS_500")'); //TODO: remove (auto)
  }
})();

/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.Contigs')
    .controller('Contigs', Contigs);

  function Contigs($scope, $rootScope, $timeout, ElasticSearch, FilterBox, MODE) {
    var contigs = this;

    contigs.mode = MODE;
    contigs.hits = [];
    contigs.total = "";
    contigs.pageSize = 10;
    contigs.pageIndexForDisplay = 1;
    contigs.after = ""; // format is usually "k141_1234"
    contigs.afterHistory = [];
    contigs.firstHistoryBackClick = true;
    contigs.loading = false;
    contigs.showVirSorterHitsOnly = false;
    contigs.showOnlyContigsLongerThan = false;
    contigs.longerThan = 1000;
    contigs.showOnlyEmptyContigs = false;
    contigs.openContigPopup = openContigPopup;
    contigs.pageBackward = pageBackward;
    contigs.pageForward = pageForward;
    contigs.addContigFilterBox = addContigFilterBox;
    contigs.onVirSorterCheckboxChange = onVirSorterCheckboxChange;
    contigs.onEmptyContigsCheckboxChange = onEmptyContigsCheckboxChange;
    contigs.onLongerThanCheckboxChange = onLongerThanCheckboxChange;
    contigs.onLongerThanChange = onLongerThanChange;
    contigs.downloadContig = downloadContig;

    var query = {"bool": {"filter": []}};
    var queryStale = true;

    function applyQuery() {
      if (queryStale) {
        contigs.after = "";
        contigs.afterHistory = [];
        fetch();
        queryStale = false;
      }
    }

    $scope.$on('ResultFilters:changed', function (event, boolQuery) {
      query = boolQuery;
      query.bool.must = {match_all: {}};  // patch query to keep scores (gene counts) when only filters are used
      queryStale = true;
      if ($scope.detailTabs.activeTabLabel === 'contigs') {
        applyQuery();
      }
    });

    $scope.$on('DetailTabs:changed', function (event, tabLabel) {
      if (tabLabel === 'contigs') {
        applyQuery();
      }
    });

    function fetch() {
      $rootScope.startSpinner();
      contigs.loading = true;
      contigs.total = "";
      $scope.$emit('DetailTabs:loading', 'contigs', true);
      ElasticSearch.getContigsPage($scope.dataset.name, query, contigs.after, contigs.pageSize, false,
        contigs.showVirSorterHitsOnly, contigs.showOnlyContigsLongerThan, contigs.longerThan, contigs.showOnlyEmptyContigs)
        .then(function (resp) {
          contigs.hits = resp.hits.hits;
          contigs.total = resp.hits.total;
          angular.forEach(contigs.hits, function (contig) {
            contigs.after = [contig._score, contig._source.length];
          });
        }, function (err) {
          console.trace(err.message);
        })
        .finally(function () {
          $scope.$emit('DetailTabs:loading', 'contigs', false);
          contigs.loading = false;
          $rootScope.stopSpinner();
        });
    }

    function pageBackward() {
      if (!contigs.loading && contigs.afterHistory.length !== 0) {
        if (contigs.firstHistoryBackClick) {
          contigs.afterHistory.pop();
        }
        contigs.after = contigs.afterHistory.pop();
        fetch();
        contigs.firstHistoryBackClick = false;
        updatePageIndex();
      }
    }

    function pageForward() {
      if (!contigs.loading && contigs.hits.length >= contigs.pageSize) {
        if (contigs.afterHistory.length === 0) {
          contigs.afterHistory.push("");
        }
        contigs.afterHistory.push(contigs.after);
        fetch();
        contigs.firstHistoryBackClick = true;
        updatePageIndex();
      }
    }

    function updatePageIndex() {
      contigs.pageIndexForDisplay = contigs.afterHistory.length + 1;
      if (contigs.firstHistoryBackClick) {
        contigs.pageIndexForDisplay--;
      }
    }

    function openContigPopup(contigid) {
      $scope.$emit('PaginatedTable:openContigPopup:relay', contigid, '-');
    }

    function addContigFilterBox(contigid) {
      var filterBox = new FilterBox('Contig: ' + contigid);
      filterBox.addFilter('contigid', contigid);
      $scope.$emit('FilterBoxes:add:relay', filterBox);
    }

    function onVirSorterCheckboxChange() {
      queryStale = true;
      applyQuery();
      contigs.pageIndexForDisplay = 1;
    }

    function onEmptyContigsCheckboxChange() {
      queryStale = true;
      applyQuery();
      contigs.pageIndexForDisplay = 1;
    }

    function onLongerThanCheckboxChange() {
      queryStale = true;
      applyQuery();
      contigs.pageIndexForDisplay = 1;
    }

    function onLongerThanChange() {
      if (contigs.showOnlyContigsLongerThan) {
        queryStale = true;
        applyQuery();
        contigs.pageIndexForDisplay = 1;
      }
    }

    function downloadContig(contigid) {
      contigs.downloadFilename = $scope.dataset.name + "__" + contigid;
      var sequence = "";
      ElasticSearch.getContig($scope.dataset.name, contigid, true, false)
        .then(function (resp) {
          sequence = resp.hits.hits[0]._source['nucleotide'];

          var downloadBlob = new Blob([">" + contigs.downloadFilename + "\n" + sequence + "\n"], {type: 'text/plain'});
          contigs.downloadUrl = (window.URL || window.webkitURL).createObjectURL(downloadBlob);
          $timeout(function () {
            document.getElementById('download-contig').click();
            (window.URL || window.webkitURL).revokeObjectURL(downloadBlob);
          }, 500);
        }, function (err) {
          console.trace(err.message);
        })
        .finally(function () {
        });
    }
  }
})();

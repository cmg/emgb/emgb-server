/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.Bins')
    .controller('Bins', Bins);

  function Bins($scope, $rootScope, ElasticSearch, FilterBox, GTDBTK_PHYLO_HIERARCHY) {
    var bins = this;

    bins.bins = [];
    bins.totalGenes = 0;
    bins.totalGenesWithBins = 0;
    bins.metadataAvailable = false;
    bins.loading = false;
    bins.GTDBTK_PHYLO_HIERARCHY = GTDBTK_PHYLO_HIERARCHY;
    bins.completenessThresholdRaw = 0;
    bins.contaminationThresholdRaw = 0;


    bins.addBinFilterBox = addBinFilterBox;
    bins.bestClassificationAndLevel = bestClassificationAndLevel;

    var query = {"bool": {"filter": []}};
    var queryStale = true;

    function applyQuery() {
      if (queryStale) {
        fetch();
        queryStale = false;
      }
    }

    $scope.$on('ResultFilters:changed', function (event, boolQuery) {
      query = boolQuery;
      queryStale = true;
      if ($scope.detailTabs.activeTabLabel === 'bins') {
        applyQuery();
      }
    });

    $scope.$on('DetailTabs:changed', function (event, tabLabel) {
      if (tabLabel === 'bins') {
        applyQuery();
      }
    });

    function fetch() {
      $rootScope.startSpinner();
      bins.loading = true;
      $scope.$emit('DetailTabs:loading', 'bins', true);
      ElasticSearch.getBinsAggregration($scope.dataset.name, query)
        .then(function (resp) {
          bins.totalGenes = resp.hits.total;
          bins.bins = resp.aggregations.binsAggregration.bins.buckets;
          bins.totalGenesWithBins = resp.aggregations.binsAggregration.doc_count;
        }, function (err) {
          console.trace(err.message);
        })
        .finally(function () {
          ElasticSearch.getBins($scope.dataset.name)
            .then(function (resp2) {
              if (resp2.hits.total > 0) {
                bins.metadataAvailable = true;
              }
              angular.forEach(resp2.hits.hits, function (binMetadata) {
                angular.forEach(bins.bins, function (bin) {
                  if (bin.key === binMetadata._source.binid) {
                    bin.metadata = binMetadata._source;
                  }
                });
              });
            }, function (err) {
              console.trace(err.message);
            })
            .finally(function () {
              $scope.$emit('DetailTabs:loading', 'bins', false);
              bins.loading = false;
              $rootScope.stopSpinner();
            });
        });
    }

    function addBinFilterBox(binId) {
      var filterBox = new FilterBox('Bin ' + binId);
      filterBox.addFilter('binning.bins', binId);
      $scope.$emit('FilterBoxes:add:relay', filterBox);
    }

    function bestClassificationAndLevel(bin) {
      var classification = "-";
      var level = "-";
      angular.forEach(GTDBTK_PHYLO_HIERARCHY, function (currentLevel) {
        if (bin.hasOwnProperty('metadata') && bin.metadata.hasOwnProperty('classification') && bin.metadata.classification[currentLevel] !== '') {
          level = currentLevel;
          classification = bin.metadata.classification[currentLevel];
        }
      });
      return [classification, level];
    }
  }
})();

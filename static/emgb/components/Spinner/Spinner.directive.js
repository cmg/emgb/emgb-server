/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2.Spinner')
    .directive("spinner", spinner);

  function spinner() {
    return {
      restrict: 'A',
      template: '<div></div>',
      controller: function ($element, $rootScope) {
        var opts = {
          lines: 9, length: 0, width: 8, radius: 18, scale: 1,
          corners: 1, color: '#666', opacity: 0.25, rotate: 0,
          direction: 1, speed: 1.6, trail: 60, fps: 20,
          zIndex: 2e9, className: 'spinner', shadow: false,
          hwaccel: true, position: 'absolute'
        };

        var spinner = new Spinner(opts).spin($element[0]);
        spinner.stop();

        $rootScope.spinnerValue = 0;

        $rootScope.startSpinner = function () {
          $rootScope.spinnerValue += 1;
          if ($rootScope.spinnerValue > 0) {
            spinner.spin($element[0]);
          }
        };

        $rootScope.stopSpinner = function () {
          $rootScope.spinnerValue -= 1;
          if ($rootScope.spinnerValue === 0) {
            spinner.stop();
          }
        };

        $rootScope.isLoading = function () {
          return $rootScope.spinnerValue > 0;
        };
      }
    };
  }
})();
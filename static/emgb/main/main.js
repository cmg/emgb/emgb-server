/* global angular */
(function () {
  'use strict';
  angular.module('emgb2', [
    'emgb2.Bins',
    'emgb2.BlastHits',
    'emgb2.Cart',
    'emgb2.CompareTab',
    'emgb2.Contigs',
    'emgb2.ContigViewer',
    'emgb2.DatasetTabs',
    'emgb2.DetailTabs',
    'emgb2.ElasticSearch',
    'emgb2.ExternalTools',
    'emgb2.FilterBoxes',
    'emgb2.GeneCountRatio',
    'emgb2.GoStats',
    'emgb2.Insights',
    'emgb2.PaginatedTable',
    'emgb2.PathwayOverview',
    'emgb2.PhyloTree',
    'emgb2.Spinner',
    'emgb2.TargetDb',
    'emgb2.TextQuery',
    'emgb2.User',
    'emgb2.Utils',
    //
    'ui.bootstrap'
  ]);


  angular.module('emgb2.Bins', []);
  angular.module('emgb2.BlastHits', []);
  angular.module('emgb2.Cart', []);
  angular.module('emgb2.CompareTab', []);
  angular.module('emgb2.Contigs', []);
  angular.module('emgb2.ContigViewer', []);
  angular.module('emgb2.DatasetTabs', []);
  angular.module('emgb2.DetailTabs', []);
  angular.module('emgb2.ElasticSearch', ['elasticsearch']);
  angular.module('emgb2.ExternalTools', []);
  angular.module('emgb2.FilterBoxes', []);
  angular.module('emgb2.GeneCountRatio', ['nvd3']);
  angular.module('emgb2.GoStats', ['nvd3']);
  angular.module('emgb2.Insights', []);
  angular.module('emgb2.PaginatedTable', ['ngResize', 'as.sortable']);
  angular.module('emgb2.PathwayOverview', ['angular.filter']);
  angular.module('emgb2.PhyloTree', ['nvd3']);
  angular.module('emgb2.Spinner', []);
  angular.module('emgb2.TargetDb', []);
  angular.module('emgb2.TextQuery', ['focus-if']);
  angular.module('emgb2.User', []);
  angular.module('emgb2.Utils', []);
})();

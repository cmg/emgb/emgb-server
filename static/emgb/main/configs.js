/* global angular */
(function () {
  'use strict';
  angular
    .module('emgb2')
    .config(function (resizeProvider) {
      resizeProvider.throttle = 50;
    })
    .config(function ($compileProvider) {
      $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|tel|file|blob):/);
    })
    .config(function ($uibTooltipProvider) {
      $uibTooltipProvider.options({
        popupDelay: 500
      });
    });
})();

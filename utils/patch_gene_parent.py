#!/usr/bin/env python

import json
import gzip
import argparse

parser = argparse.ArgumentParser(description='Add contigid as _parent to gene objects.')
parser.add_argument("--input", dest='input_json_gz', required=True, help='json.gz input file')
parser.add_argument("--output", dest='output_json_gz', required=True, help='json.gz output file')
args = parser.parse_args()

with gzip.open(args.input_json_gz, 'rb') as input:
    with gzip.open(args.output_json_gz, 'wb') as output:
        for line in input:
            header = {"index": {"_parent": json.loads(line)['contigid']}}
            output.write(json.dumps(header, separators=(',', ':')).encode('utf-8'))
            output.write(b'\n')
            output.write(line.strip())
            output.write(b'\n')
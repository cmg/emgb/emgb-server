#!/usr/bin/env python

import json
import gzip
import argparse
import csv

parser = argparse.ArgumentParser(description='Add binning data to gene objects.')
parser.add_argument("--input", dest='input_json_gz', required=True, help='json.gz input file')
parser.add_argument("--output", dest='output_json_gz', required=True, help='json.gz output file')
parser.add_argument("--bins-tsv", dest='bins_tsv', required=True, help='json.gz output file')
parser.add_argument("--binning-label", dest='label', required=True, help='json.gz output file')
args = parser.parse_args()
label = args.label

binmap = {}

with open(args.bins_tsv, 'rt') as file:
  bins_tsv_reader = csv.reader(file, delimiter='\t')
  for row in bins_tsv_reader:
    binmap[row[0].strip()] = row[1].strip()

with gzip.open(args.input_json_gz, 'rb') as inputJson:
  with gzip.open(args.output_json_gz, 'wb') as outputJson:
    is_header = True
    for line in inputJson:
      if is_header:
        outputJson.write(line.strip())
        outputJson.write(b'\n')
      else:
        gene = json.loads(line)
        if gene['contigid'] in binmap:
          if 'binning' not in gene:
            gene['binning'] = []
          gene['binning'].append({'label': label, 'bins': [binmap[gene['contigid']]]})
        outputJson.write(json.dumps(gene, separators=(',', ':')).encode('utf-8'))
        outputJson.write(b'\n')
      is_header = not is_header

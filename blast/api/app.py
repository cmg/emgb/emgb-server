from apistar import Route
from apistar.http import RequestData, JSONResponse
from apistar import types, validators
from apistar import App
from Bio.Blast.Applications import NcbiblastpCommandline, NcbiblastxCommandline
from Bio.Application import ApplicationError
from Bio.Blast import NCBIXML
from tempfile import NamedTemporaryFile
import multiprocessing
import os

DB_PATH = "/var/lib/blast/db/db"
url_prefix = os.environ['URL_PREFIX']


def system_info():
  data = {
    'cores': multiprocessing.cpu_count(),
    'db_info': {
      'last_modified': int(os.path.getmtime('%s.faa' % DB_PATH))
    }
  }
  return JSONResponse(data, status_code=200, headers={'connection': 'close'})


class BlastRequest(types.Type):
  seq_type = validators.String(enum=['p', 'x'])
  query = validators.String(max_length=1000000)
  return_alignment = validators.Boolean(default=False)
  limit = validators.Integer(minimum=1, maximum=1000, default=25)
  evalue_cutoff = validators.Number(minimum=0.0, maximum=100.0, default=0.001)


def run_blast(req: BlastRequest):
  if req.query == '':
    return {'hits': []}
  with NamedTemporaryFile(mode='w+') as query_faa:
    if not req.query.startswith('>'):
      query_faa.file.write('>query_sequence\n')
    query_faa.file.write(req.query)
    query_faa.file.write('\n')
    query_faa.file.flush()
    with NamedTemporaryFile(mode='w+') as result_xml:
      if req.seq_type == 'p':
        blast_class = NcbiblastpCommandline
      else:
        blast_class = NcbiblastxCommandline
      blast_cmd = blast_class(query=query_faa.name, db=DB_PATH, outfmt=5,
                              evalue=req.evalue_cutoff, max_target_seqs=req.limit,
                              out=result_xml.name, num_threads=multiprocessing.cpu_count())
      try:
        blast_cmd()
        blast_record = NCBIXML.read(result_xml)
        return JSONResponse({
          'hits': [
            {
              'id': a.hit_def.split(' ')[0],
              'evalue': a.hsps[0].expect,
              'score': a.hsps[0].score,
              'query': a.hsps[0].query if req.return_alignment else '',
              'match': a.hsps[0].match if req.return_alignment else '',
              'subject': a.hsps[0].sbjct if req.return_alignment else '',
              'query_start': a.hsps[0].query_start,
              'subject_start': a.hsps[0].sbjct_start,
              'query_end': a.hsps[0].query_end,
              'subject_end': a.hsps[0].sbjct_end
            }
            for a in blast_record.alignments[:req.limit]]
        }, status_code=200, headers={'connection': 'close'})
      except ApplicationError as ae:
        return JSONResponse({'error': str(ae.stderr.split('\n', 1)[0])}, status_code=422,
                            headers={'connection': 'close'})
      except ValueError as ve:
        return JSONResponse({'error': str(ve.args)}, status_code=422, headers={'connection': 'close'})
      except:
        return JSONResponse({'error': 'Bad Request'}, status_code=400, headers={'connection': 'close'})


routes = [
  Route('%s/run_blast' % url_prefix, 'POST', run_blast),
  Route('%s/system_info' % url_prefix, 'GET', system_info)
]

application = App(routes=routes)

if __name__ == '__main__':
  application.main()

#!/bin/bash
echo "Downloading pathway data from bibiserv ..."
wget -nv -O- https://bibiserv.cebitec.uni-bielefeld.de/resources/emgb2/emgb.pathways.json.gz > import/emgb.pathways.json.gz \
&& echo "Done" || echo "Failed"